# 2018.4.30-alpha
- Close warning
- Navigation improvements
- Soundbank enhancements
- Gamaka scaling
- Java upgrade

# 2018.4.1-alpha
- Gamaka categories
- Background drone
- Lead play sequencer & cues
- Music: Shankarabharanam varnam
- Bugfix: Multi channel race condition
- Bugfix: Active swara cell rendering fix
- Bugfix: Swara area scrolling

# 2018.3.17-alpha
- Shit & Select
- Select all
- Editable raagas
- Post swara edit
- Gamaka Descriptions & Report
- Gamaka shapes as SVG
- Suggested Gamakas

# 2018.3.3-alpha
- Grid based swara text area 
- Raaga definitions
- Keyboard events (arrows, note input)
- Copy & Paste
- Song & Gamaka File formats
- Smooth bezier interpolation
- Synchronized play & pause
- Gamaka shapes in UI