package net.stmdhr.gamidi.swara;

public enum Note {

	S('S', 0), //
	R('R', 1), //
	G('G', 2), //
	M('M', 3), //
	P('P', 4), //
	D('D', 5), //
	N('N', 6), //
	REST('.', 7)//
	;

	private final char symbol;
	private final Integer value;

	Note(char symbol, Integer value) {
		this.symbol = symbol;
		this.value = value;
	}

	public char getSymbol() {
		return symbol;
	}

	public Integer getValue() {
		return value;
	}

	public String toString() {
		return this.name();
	}

	public static Note fromSymbol(char symbol) {
		symbol = Character.toUpperCase(symbol);
		Note[] values = values();
		for (Note note : values) {
			if (note.symbol == symbol) {
				return note;
			}
		}
		return null;
	}
	
	public static Note fromValue(int value) {
		Note[] values = values();
		for (Note note : values) {
			if (note.value == value) {
				return note;
			}
		}
		return S;
	}
}
