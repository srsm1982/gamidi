package net.stmdhr.gamidi.swara;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

public enum Duration {

	QUARTER("\u00BC", 0.25), //
	HALF("\u00BD", 0.5), //
	FULL("1", 1.0), //
	ONEANDHALF("1\u00BD", 1.5), 
	DOUBLE("2", 2.0), //
	TRIPLE("3", 3.0), //
	QUADRUPLE("4", 4.0), //
	;

	private final String symbol;
	private final Double value;

	Duration(String symbol, Double value) {
		this.symbol = symbol;
		this.value = value;
	}

	public String getSymbol() {
		return symbol;
	}

	@JsonValue
	public Double getValue() {
		return value;
	}

	public static Duration fromSymbol(String symbol) {
		Duration[] values = values();
		for (Duration duration : values) {
			if (duration.symbol.equals(symbol)) {
				return duration;
			}
		}
		return FULL;
	}

	@JsonCreator
	public static Duration fromValue(String valueStr) {
		double value = Double.valueOf(valueStr);
		Duration[] values = values();
		for (Duration duration : values) {
			if (duration.value == value) {
				return duration;
			}
		}
		return FULL;
	}

	public Duration getNextDuration() {
		int ordinal = ordinal();
		Duration[] values = values();
		if (ordinal < values.length - 1) {
			ordinal++;
		}
		return values[ordinal];
	}

	public Duration getPreviousDuration() {
		int ordinal = ordinal();
		Duration[] values = values();
		if (ordinal > 0) {
			ordinal--;
		}
		return values[ordinal];
	}
}
