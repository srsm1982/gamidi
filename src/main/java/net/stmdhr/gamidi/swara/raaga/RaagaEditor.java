package net.stmdhr.gamidi.swara.raaga;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Settings;
import net.stmdhr.gamidi.data.RaagaIO;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.gamaka.GamakaEditor;

public class RaagaEditor extends Application {
	private static final Insets PADDING_5 = new Insets(5);
	private static final Logger LOGGER = LoggerFactory.getLogger(GamakaEditor.class);

	private StringProperty idProperty = new SimpleStringProperty();
	private StringProperty nameProperty = new SimpleStringProperty();
	private StringProperty descriptionProperty = new SimpleStringProperty();
	private StringProperty semitonesProperty = new SimpleStringProperty();
	private StringProperty[] swaraProperty = new SimpleStringProperty[7];

	private StringProperty messageProperty = new SimpleStringProperty();

	public RaagaEditor() {
		for (int i = 0; i < 7; i++) {
			swaraProperty[i] = new SimpleStringProperty();
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox root = new VBox(5);

		{
			HBox row = new HBox(5);
			row.setPadding(PADDING_5);
			root.getChildren().add(row);

			{
				Button button = new Button("New");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						idProperty.set("-1");
						nameProperty.set("New");
						descriptionProperty.set("");
						semitonesProperty.set("");
						for (int i = 0; i < 7; i++) {
							swaraProperty[i].set("");
						}
						messageProperty.set("New raaga");
					}
				});
				row.getChildren().add(button);
			}

			{
				Button button = new Button("Open");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {

						try {
							FileChooser chooser = new FileChooser();
							chooser.setTitle("Open Raaga file");

							String raagaDir = Settings.getRaagaDirectory();
							chooser.setInitialDirectory(new File(raagaDir));
							chooser.getExtensionFilters().add(Settings.RAAGA_FILTER);

							File file = chooser.showOpenDialog(primaryStage);
							if (file != null) {

								String content = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
								Raaga raaga = RaagaIO.readRaaga(content);

								idProperty.set(raaga.getId());
								nameProperty.set(raaga.getName());
								descriptionProperty.set(raaga.getDescription());

								String semitonesString = convertToString(raaga.getSemitones());

								semitonesProperty.set(semitonesString);
								for (int i = 0; i < 7; i++) {
									swaraProperty[i].set(convertToString(raaga.getSuggestedGamakasMap().get(i)));
								}
								messageProperty.set("Loaded from:" + file.getAbsolutePath());
							}
						} catch (Exception e) {
							LOGGER.error("Error loading file", e);
						}
					}
				});
				row.getChildren().add(button);
			}

			{
				Button button = new Button("Save");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						try {

							String id = idProperty.get();
							String name = nameProperty.get();
							String description = descriptionProperty.get();
							String semitones = semitonesProperty.get();
							int[] semitonesIntArray = ArrayUtils.toPrimitive(convertToArray(semitones));

							String[] swaraGamakas = new String[7];
							for (int i = 0; i < 7; i++) {
								swaraGamakas[i] = swaraProperty[i].get();
							}

							Map<Integer, List<Integer>> map = new HashMap<>();
							for (int i = 0; i < 7; i++) {
								Integer[] intArray = convertToArray(swaraGamakas[i]);
								List<Integer> intList = new ArrayList<>();
								CollectionUtils.addAll(intList, intArray);
								map.put(i, intList);
							}

							Raaga raaga = new Raaga(id, name, description, semitonesIntArray, map);
							String content = RaagaIO.write(raaga);

							FileChooser chooser = new FileChooser();
							chooser.setTitle("Save Raaga file");

							String raagaDir = Settings.getRaagaDirectory();
							chooser.setInitialDirectory(new File(raagaDir));
							chooser.getExtensionFilters().add(Settings.RAAGA_FILTER);

							File file = chooser.showSaveDialog(primaryStage);
							if (file != null) {
								FileUtils.write(file, content, Charset.forName("UTF-8"), false);
								messageProperty.set("Saved to: " + file.getAbsolutePath());
							}

						} catch (Exception e) {
							LOGGER.error("Error writing gamaka file", e);
						}
					}
				});
				row.getChildren().add(button);
			}
		}

		{
			HBox hbox = new HBox(5);
			root.getChildren().add(hbox);
			{
				GridPane grid = new GridPane();
				grid.setPadding(PADDING_5);
				grid.setHgap(5);
				grid.setVgap(5);
				hbox.getChildren().add(grid);

				{
					Label label = new Label("Id");
					grid.add(label, 0, 0);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), idProperty);
					grid.add(field, 1, 0);
				}

				{
					Label label = new Label("Name");
					grid.add(label, 0, 1);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), nameProperty);
					grid.add(field, 1, 1);
				}

				{
					Label label = new Label("Description");
					grid.add(label, 0, 2);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), descriptionProperty);
					grid.add(field, 1, 2);
				}

				{
					Label label = new Label("Semitones");
					grid.add(label, 0, 3);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), semitonesProperty);
					grid.add(field, 1, 3);
				}

				{
					Label label = new Label("Raaga Gamakas");
					grid.add(label, 0, 4);

					Note[] notes = Note.values();
					for (int i = 0; i < 7; i++) {
						String noteValue = notes[i].toString();
						Label label1 = new Label(noteValue);
						grid.add(label1, 0, i+5);

						TextField field = new TextField();
						Bindings.bindBidirectional(field.textProperty(), swaraProperty[i]);
						grid.add(field, 1, i + 5);
					}

				}

			}

			{
				TextArea message = new TextArea();
				message.setPadding(PADDING_5);
				message.setPrefWidth(200);
				message.setPrefHeight(50);
				message.setWrapText(true);
				message.setEditable(false);
				message.getStyleClass().add("message");
				message.textProperty().bind(messageProperty);
				root.getChildren().add(message);
			}

		}

		Scene scene = new Scene(root);
		scene.getStylesheets().add("/net/stmdhr/gamidi/application.css");
		primaryStage.setScene(scene);

		primaryStage.setTitle("Raaga Editor");
		primaryStage.setMaximized(true);
		primaryStage.show();

	}

	private Integer[] convertToArray(String array) {
		String[] strValues = array.split(",");
		Integer[] intValues = new Integer[strValues.length];
		for (int i = 0; i < strValues.length; i++) {
			intValues[i] = Integer.valueOf(strValues[i].trim());
		}
		return intValues;
	}

	private String convertToString(int[] array) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			builder.append(array[i]);
			if (i != array.length - 1) {
				builder.append(",");
			}
		}
		return builder.toString();
	}

	private String convertToString(List<Integer> list) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < list.size(); i++) {
			builder.append(list.get(i));
			if (i != list.size() - 1) {
				builder.append(",");
			}
		}
		return builder.toString();
	}
}
