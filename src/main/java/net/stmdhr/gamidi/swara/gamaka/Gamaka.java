package net.stmdhr.gamidi.swara.gamaka;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Settings;
import net.stmdhr.gamidi.data.GamakaIO;
import net.stmdhr.gamidi.swara.Duration;
import net.stmdhr.gamidi.swara.Note;

public class Gamaka {

	public static final double RANGE = 4;
	public static final int NO_GAMAK_ID = -1;
	public static final Gamaka NO_GAMAKA = new Gamaka(NO_GAMAK_ID, "No gamaka", "-", "No Gamaka.", GamakaCategory.NONE,
			GamakaScale.BASE, null);

	public static final ShortMessage RESET_PITCH_MESSAGE = createPitchBendMessage(8192);

	private static final Logger LOGGER = LoggerFactory.getLogger(Gamaka.class);

	private static final Map<Integer, Gamaka> VALUES_MAP = new HashMap<>();
	private static final List<Integer> ID_LIST = new ArrayList<>();
	private static final List<Gamaka> GAMAKA_LIST = new ArrayList<>();

	private final int id;
	private final String name;
	private final String description;
	private final String symbol;
	private final List<PitchPoint> pitchPoints;
	private final GamakaCategory category;
	private final GamakaScale scale;
	private final double[] noteBends;
	private final UnivariateFunction noteBendFunction;

	static {
		loadFromFileSystem();
	}

	public Gamaka(int id, String name, String symbol, String description, GamakaCategory category, GamakaScale scale,
			List<PitchPoint> pitchPoints) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.symbol = symbol;
		this.category = category;
		this.pitchPoints = pitchPoints;
		this.scale = scale;

		if (pitchPoints != null) {
			noteBends = calculateNoteBends(pitchPoints);

			double x[] = new double[Player.PLAYER_RESOLUTION];
			for (int i = 0; i < Player.PLAYER_RESOLUTION; i++) {
				x[i] = i;
			}

			LinearInterpolator interpolator = new LinearInterpolator();
			noteBendFunction = interpolator.interpolate(x, noteBends);

		} else {
			noteBends = null;
			noteBendFunction = null;
		}
	}

	private static class Point {
		double x;
		double y;

		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}

		public Point(PitchPoint p) {
			this.x = p.getTick();
			this.y = p.getNoteBend();
		}
	}

	private static final double R = 0.7;
	private static final double IR = 1.0 - R;
	private static final double STEP = 0.1;

	public static double[] calculateNoteBends(List<PitchPoint> pitchPoints) {

		double[] noteBends = new double[Player.PLAYER_RESOLUTION];
		try {

			int pitchPointsSize = pitchPoints.size();
			List<Point> interPoints = new ArrayList<>();

			if (pitchPointsSize > 2) {

				// Add first
				interPoints.add(new Point(pitchPoints.get(0)));

				for (int i = 0; i <= pitchPointsSize - 3; i++) {

					// Left, middle, right
					Point l = new Point(pitchPoints.get(i));
					Point m = new Point(pitchPoints.get(i + 1));
					Point r = new Point(pitchPoints.get(i + 2));

					// left <R-IR> middle, middle, middle <IR-R> right
					Point p0 = new Point(l.x * IR + m.x * R, l.y * IR + m.y * R);
					Point p1 = m;
					Point p2 = new Point(m.x * R + r.x * IR, m.y * R + r.y * IR);

					// Quadratic Bezier
					for (double t = 0; t <= 1; t = t + STEP) {

						// Coefficients
						double a = Math.pow(1 - t, 2);
						double b = 2 * (1 - t) * t;
						double c = Math.pow(t, 2);

						// Bezier Point
						Point p = new Point(a * p0.x + b * p1.x + c * p2.x, a * p0.y + b * p1.y + c * p2.y);
						interPoints.add(p);
					}
				}

				// Add last
				interPoints.add(new Point(pitchPoints.get(pitchPointsSize - 1)));

			} else {

				// Size is less than 3; add all points
				for (int i = 0; i <= pitchPointsSize - 1; i++) {
					interPoints.add(new Point(pitchPoints.get(i)));
				}
			}

			int interPointsSize = interPoints.size();

			double[] x = new double[interPointsSize];
			double[] y = new double[interPointsSize];

			for (int i = 0; i < interPointsSize; i++) {
				Point interPoint = interPoints.get(i);
				x[i] = interPoint.x;
				y[i] = interPoint.y;
			}

			LinearInterpolator interpolator = new LinearInterpolator();
			UnivariateFunction function = interpolator.interpolate(x, y);

			for (int i = 0; i < Player.PLAYER_RESOLUTION; i++) {
				noteBends[i] = function.value(i);
			}

		} catch (Exception e) {

			LOGGER.error("Error calculating notebends. pitchPoints={}", pitchPoints, e);
			Arrays.fill(noteBends, 0);
		}

		return noteBends;
	}

	private static void loadFromFileSystem() {

		VALUES_MAP.clear();
		ID_LIST.clear();
		GAMAKA_LIST.clear();

		VALUES_MAP.put(NO_GAMAKA.id, NO_GAMAKA);

		String gamakaDir = Settings.getGamakaDirectory();
		LOGGER.debug("Loading gamakas from gamakaDir={}", gamakaDir);

		File folder = new File(gamakaDir);
		File[] files = folder.listFiles();
		for (File file : files) {
			String fileName = file.getName();
			if (file.isFile() && fileName.endsWith(".gmk.json")) {
				try {
					String content = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
					Gamaka gamaka = GamakaIO.readGamaka(content);
					VALUES_MAP.put(gamaka.id, gamaka);
				} catch (Exception e) {
					LOGGER.error("Error parsing fileName={}", fileName, e);
				}
			}
		}

		ID_LIST.addAll(VALUES_MAP.keySet());
		GAMAKA_LIST.addAll(VALUES_MAP.values());
	}

	public double[] getNoteBends() {
		return noteBends;
	}

	public void addMidiEvents(Track track, Note note, Raaga raaga, Duration duration, long startTick) {

		if (noteBends == null) {
			return;
		}

		double durationValue = duration.getValue();
		long tickRange = (long) (durationValue * Player.PLAYER_RESOLUTION);
		long endTick = startTick + tickRange;

		for (long i = startTick; i < endTick; i++) {

			int scaledTick = (int) (Player.PLAYER_RESOLUTION * (i - startTick) / tickRange);
			double noteBend = noteBendFunction.value(scaledTick);
			int pitchBend = raaga.getPitchBend(note, noteBend);
			ShortMessage pitchMessage = createPitchBendMessage(pitchBend);

			MidiEvent pitchEvent = new MidiEvent(pitchMessage, i);
			track.add(pitchEvent);
		}

		MidiEvent resetEvent = new MidiEvent(Gamaka.RESET_PITCH_MESSAGE, endTick - 1);
		track.add(resetEvent);
	}

	private static ShortMessage createPitchBendMessage(int pitchBend) {

		ShortMessage pitchMessage = new ShortMessage();
		try {
			int bendMSB = pitchBend >> 7;
			int bendLSB = pitchBend & 0b1111111;
			pitchMessage.setMessage(ShortMessage.PITCH_BEND, 0, bendLSB, bendMSB);

		} catch (Exception e) {
			LOGGER.error("Error creating pitch bend message. pitchBend={}", pitchBend, e);
		}
		return pitchMessage;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSymbol() {
		return symbol;
	}

	public GamakaCategory getCategory() {
		return category;
	}

	public List<PitchPoint> getPitchPoints() {
		return pitchPoints;
	}

	public String getDescription() {
		return description;
	}

	public static List<Gamaka> values() {
		return GAMAKA_LIST;
	}

	public static List<Integer> ids() {
		return ID_LIST;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gamaka other = (Gamaka) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public static Gamaka valueOf(int id) {
		final Gamaka value = VALUES_MAP.get(id);
		if (value != null) {
			return value;
		} else {
			LOGGER.error("Could not find gamaka for id={}", id);
			return NO_GAMAKA;
		}
	}

	public String getOccurence() {
		StringBuilder builder = new StringBuilder();
		List<Raaga> raagas = Raaga.values();
		for (Raaga raaga : raagas) {
			Map<Integer, List<Integer>> suggestedGamakasMap = raaga.getSuggestedGamakasMap();
			Set<Integer> notes = suggestedGamakasMap.keySet();
			for (Integer noteValue : notes) {
				List<Integer> suggestedGamakas = suggestedGamakasMap.get(noteValue);
				if (suggestedGamakas.contains(this.id)) {
					builder.append(raaga.getName() + ": " + Note.fromValue(noteValue).getSymbol() + "\n");
				}
			}
		}
		return builder.toString();
	}

	public static List<Gamaka> getGamakas(GamakaCategory category) {
		List<Gamaka> gamakas = new ArrayList<>();
		for (Gamaka gamaka : GAMAKA_LIST) {
			if (category.equals(gamaka.category)) {
				gamakas.add(gamaka);
			}
		}
		return gamakas;
	}

	public GamakaScale getScale() {
		return scale;
	}
}