package net.stmdhr.gamidi.swara.gamaka;

import net.stmdhr.gamidi.Player;

public class PitchPoint {

	private int tick;
	private double noteBend;
	
	public static final PitchPoint LEFT_ZERO = new PitchPoint(0, 0);
	public static final PitchPoint RIGHT_ZERO = new PitchPoint(Player.PLAYER_RESOLUTION, 0);
	
	public PitchPoint() {
	}
	
	public PitchPoint(int tick, double noteBend) {
		this.tick = tick;
		this.noteBend = noteBend;
	}

	public int getTick() {
		return tick;
	}

	public void setTick(int tick) {
		this.tick = tick;
	}

	public double getNoteBend() {
		return noteBend;
	}

	public void setNoteBend(double noteBend) {
		this.noteBend = noteBend;
	}
}