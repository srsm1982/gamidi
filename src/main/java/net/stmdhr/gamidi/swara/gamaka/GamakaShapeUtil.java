package net.stmdhr.gamidi.swara.gamaka;

import net.stmdhr.gamidi.Player;

public class GamakaShapeUtil {

	public static String calculateAxesPath(double sizeX, double sizeY, int divisions) {
		
		double divOnXaxis = sizeX / divisions;
		double divOnYaxis = sizeY / divisions;
	
		StringBuilder path = new StringBuilder();
		path.append("M0,0" + " " + "h" + sizeX + " v" + sizeY + " h" + (-sizeX) + " v" + (-sizeY));
	
		// horizontal lines
		for (int i = 1; i < divisions; i++) {
			if (i % 2 == 0) {
				path.append(" m0," + divOnYaxis + " h" + (-sizeX));
			} else {
				path.append(" m0," + divOnYaxis + " h" + sizeX);
			}
		}
	
		// vertical lines
		path.append("M" + divOnXaxis + ",0 v" + (sizeY));
		for (int i = 0; i < divisions - 1; i++) {
			if (i % 2 == 0) {
				path.append(" m" + divOnXaxis + ",0 v" + (-sizeY));
			} else {
				path.append(" m" + divOnXaxis + ",0 v" + (sizeY));
			}
		}
	
		return path.toString();
	}

	public static String calculateGamakaPath(double[] noteBends, double sizeX, double sizeY) {
		double scaleX = sizeX / Player.PLAYER_RESOLUTION;
		double scaleY = sizeY / (2 * Gamaka.RANGE);
	
		StringBuilder builder = new StringBuilder();
		builder.append("M0," + scaleY * Gamaka.RANGE + " ");
		if (noteBends != null) {
			for (int i = 0; i < noteBends.length; i++) {
				double noteBend = noteBends[i];
				builder.append("L" + scaleX * i + "," + scaleY * (Gamaka.RANGE - noteBend) + " ");
			}
		} else {
			builder.append("L" + scaleX * Player.PLAYER_RESOLUTION + "," + scaleY * Gamaka.RANGE + " ");
		}
	
		return builder.toString();
	}

}
