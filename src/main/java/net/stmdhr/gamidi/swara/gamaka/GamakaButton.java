package net.stmdhr.gamidi.swara.gamaka;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.song.GamakaGroup;

public class GamakaButton {

	public static final Text SUGGESTED_HEADING = new Text("Suggested");
	public static final Text OTHER_HEADING = new Text("Other");

	private static final GamakaGroup TOGGLE_GROUP = GamakaGroup.getInstance();
	private static final Map<Integer, GamakaButton> VALUES_MAP = new HashMap<>();

	private final ToggleButton toggle;
	private final Node shape;

	static {
		loadFromGamakas();
	}

	private static void loadFromGamakas() {
		VALUES_MAP.clear();
		Collection<Gamaka> gamakas = Gamaka.values();
		for (Gamaka gamaka : gamakas) {
			int id = gamaka.getId();
			GamakaButton button = new GamakaButton(gamaka);
			VALUES_MAP.put(id, button);
		}
	}

	public static ToggleButton getToggleButton(int id) {
		GamakaButton gamakaButton = VALUES_MAP.get(id);
		return gamakaButton.toggle;
	}
	
	public static GamakaButton getGamakaButton(int id) {
		GamakaButton gamakaButton = VALUES_MAP.get(id);
		return gamakaButton;
	}

	public static GamakaGroup getGroup() {
		return TOGGLE_GROUP;
	}

	public Node getShape() {
		return shape;
	}

	private GamakaButton(Gamaka gamaka) {
		double[] noteBends = gamaka.getNoteBends();
		String symbol = gamaka.getSymbol();

		this.toggle = new ToggleButton();
		toggle.setText(symbol);
		toggle.setPrefWidth(35);
		toggle.setUserData(gamaka);
		toggle.setToggleGroup(TOGGLE_GROUP);
		toggle.setPadding(Style.PADDING_5);
		toggle.setMaxWidth(Double.MAX_VALUE);
		
		this.shape = createShape(noteBends, 300, 100);
	}

	private Node createShape(double[] noteBends, double sizeX, double sizeY) {
		SVGPath shapePath = (SVGPath) calculateGamakaShape(noteBends, sizeX, sizeY);
		SVGPath axesPath = (SVGPath) calculateAxesShape(sizeX, sizeY, 4);

		Group group = new Group();
		group.getChildren().add(shapePath);
		group.getChildren().add(axesPath);

		return group;
	}

	private Shape calculateGamakaShape(double[] noteBends, double sizeX, double sizeY) {
		SVGPath shape = new SVGPath();
		String svgPath = GamakaShapeUtil.calculateGamakaPath(noteBends, sizeX, sizeY);

		shape.setContent(svgPath);
		shape.setStroke(Color.BLACK);
		shape.setStrokeWidth(4.0);
		shape.setFill(null);
		return shape;
	}

	private Shape calculateAxesShape(double sizeX, double sizeY, int divisions) {
		SVGPath axes = new SVGPath();
		String svgPath = GamakaShapeUtil.calculateAxesPath(sizeX, sizeY, divisions);
		axes.setContent(svgPath);
		axes.setFill(null);
		axes.setStroke(Color.RED);
		axes.setStrokeWidth(0.5);
		return axes;
	}

}
