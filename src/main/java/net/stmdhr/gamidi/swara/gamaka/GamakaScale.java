package net.stmdhr.gamidi.swara.gamaka;

public enum GamakaScale {

	THREE_PLUS_DOWN("\u2193 3+"), //
	TWO_DOWN("\u2193 2"), //
	ONE_DOWN("\u2193 1"), //
	BASE("\u2194"), //
	ONE_UP("\u2191 1"), //
	TWO_UP("\u2191 2"), //
	THREE_PLUS_UP("\u2191 3+") //
	;
	
	private final String text;
	
	private GamakaScale(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
}
