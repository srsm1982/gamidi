
package net.stmdhr.gamidi.swara.gamaka;

import java.io.File;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.Instrument;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Settings;
import net.stmdhr.gamidi.Shruti;
import net.stmdhr.gamidi.ShrutiOctave;
import net.stmdhr.gamidi.data.GamakaIO;
import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.Duration;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.Octave;
import net.stmdhr.gamidi.swara.Swara;

public class GamakaEditor extends Application {

	private static final Insets PADDING_5 = new Insets(5);
	private static final Logger LOGGER = LoggerFactory.getLogger(GamakaEditor.class);

	private Player player = Player.getInstance();

	private StringProperty idProperty = new SimpleStringProperty();
	private StringProperty nameProperty = new SimpleStringProperty();
	private StringProperty symbolProperty = new SimpleStringProperty();
	private StringProperty descriptionProperty = new SimpleStringProperty();
	private ObjectProperty<GamakaCategory> categoryProperty = new SimpleObjectProperty<>();
	private ObjectProperty<GamakaScale> scaleProperty = new SimpleObjectProperty<>();
	private StringProperty pitchPointsProperty = new SimpleStringProperty();

	private ObjectProperty<Note> noteProperty = new SimpleObjectProperty<>();
	private StringProperty messageProperty = new SimpleStringProperty();

	public GamakaEditor() {

		// Set defaults
		Song song = Song.getInstance();
		song.setShruti(Shruti.G1);
		song.setShrutiOctave(ShrutiOctave.S4);
		song.setTempo(200);
		Instrument[] availableInstruments = player.getAvailableInstruments();
		for (Instrument instrument : availableInstruments) {
			String name = instrument.getName().trim();
			if ("Violin".equals(name)) {
				song.setInstrument(instrument);
				break;
			}
		}
	}

	@Override
	public void start(Stage stage) {

		VBox root = new VBox(5);

		{
			HBox row = new HBox(5);
			row.setPadding(PADDING_5);
			root.getChildren().add(row);

			{
				Button button = new Button("New");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						idProperty.set("-100");
						nameProperty.set("New");
						symbolProperty.set("N");
						pitchPointsProperty.set("");
						categoryProperty.set(GamakaCategory.NONE);
						messageProperty.set("New gamaka");
					}
				});
				row.getChildren().add(button);

				button.fire();
			}

			{
				Button button = new Button("Open");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {

						try {
							FileChooser chooser = new FileChooser();
							chooser.setTitle("Open Gamaka file");

							String gamakaDir = Settings.getGamakaDirectory();
							chooser.setInitialDirectory(new File(gamakaDir));
							chooser.getExtensionFilters().add(Settings.GAMAKA_FILTER);

							File file = chooser.showOpenDialog(stage);
							if (file != null) {

								String content = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
								Gamaka gamaka = GamakaIO.readGamaka(content);

								idProperty.set(String.valueOf(gamaka.getId()));
								nameProperty.set(gamaka.getName());
								symbolProperty.set(gamaka.getSymbol());
								descriptionProperty.set(gamaka.getDescription());
								categoryProperty.set(gamaka.getCategory());
								scaleProperty.set(gamaka.getScale());
								List<PitchPoint> pitchPoints = gamaka.getPitchPoints();
								pitchPointsProperty.set(convertToCsv(pitchPoints));

								messageProperty.set("Loaded from:" + file.getAbsolutePath());
							}
						} catch (Exception e) {
							LOGGER.error("Error loading file", e);
						}
					}
				});
				row.getChildren().add(button);
			}

			{
				Button button = new Button("Save");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						try {
							int id = Integer.valueOf(idProperty.get());
							String name = nameProperty.get();
							String symbol = symbolProperty.get();
							String csv = pitchPointsProperty.get();
							String desc = descriptionProperty.get();
							GamakaCategory category = categoryProperty.getValue();
							GamakaScale scale = scaleProperty.get();
							List<PitchPoint> pitchPoints = convertToList(csv);

							Gamaka gamaka = new Gamaka(id, name, symbol, desc, category, scale, pitchPoints);
							String content = GamakaIO.write(gamaka);

							FileChooser chooser = new FileChooser();
							chooser.setTitle("Save Gamaka file");

							String gamakaDir = Settings.getGamakaDirectory();
							chooser.setInitialDirectory(new File(gamakaDir));
							chooser.getExtensionFilters().add(Settings.GAMAKA_FILTER);

							File file = chooser.showSaveDialog(stage);
							FileUtils.write(file, content, Charset.forName("UTF-8"), false);

							messageProperty.set("Saved to: " + file.getAbsolutePath());

						} catch (Exception e) {
							LOGGER.error("Error writing gamaka file", e);
						}
					}
				});
				row.getChildren().add(button);
			}

		}

		{
			HBox hbox = new HBox(5);
			hbox.setPadding(PADDING_5);
			root.getChildren().add(hbox);

			{
				ChoiceBox<Raaga> choice = new ChoiceBox<>();
				choice.setConverter(new StringConverter<Raaga>() {

					@Override
					public String toString(Raaga raaga) {
						return raaga.getName() + "\n" + raaga.getDescription();
					}

					@Override
					public Raaga fromString(String name) {
						return null;
					}
				});
				choice.getItems().addAll(Raaga.values());
				Song.getInstance().getRaagaProperty().bind(choice.valueProperty());
				choice.setValue(Raaga.DEFAULT);
				hbox.getChildren().add(choice);
			}

			{
				ChoiceBox<Note> choice = new ChoiceBox<>();
				choice.setConverter(new StringConverter<Note>() {

					@Override
					public String toString(Note note) {
						return note.name();
					}

					@Override
					public Note fromString(String name) {
						return Note.valueOf(name);
					}
				});
				choice.getItems().addAll(Note.values());
				noteProperty.bind(choice.valueProperty());
				choice.setValue(Note.S);
				hbox.getChildren().add(choice);
			}

			{
				Button button = new Button("Test");
				button.setPadding(PADDING_5);
				button.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						int id = Integer.valueOf(idProperty.get());
						String name = nameProperty.get();
						String symbol = symbolProperty.get();
						String csv = pitchPointsProperty.get();
						String desc = descriptionProperty.get();
						GamakaCategory category = categoryProperty.get();
						GamakaScale scale = scaleProperty.get();

						List<PitchPoint> pitchPoints = convertToList(csv);
						Gamaka gamaka = new Gamaka(id, name, symbol, desc, category, scale, pitchPoints);
						Swara swara = new Swara();
						swara.setNote(noteProperty.get());
						swara.setGamaka(gamaka);
						swara.setDuration(Duration.DOUBLE);
						swara.setOctave(Octave.BASE);

						player.playLead(swara);
					}
				});
				hbox.getChildren().add(button);
			}
		}

		{
			HBox hbox = new HBox(5);
			root.getChildren().add(hbox);

			{
				GridPane grid = new GridPane();
				grid.setPadding(PADDING_5);
				grid.setHgap(5);
				grid.setVgap(5);
				hbox.getChildren().add(grid);

				{
					Label label = new Label("Id");
					grid.add(label, 0, 1, 1, 1);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), idProperty);
					grid.add(field, 1, 1, 1, 1);
				}

				{
					Label label = new Label("Name");
					grid.add(label, 0, 2, 1, 1);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), nameProperty);
					grid.add(field, 1, 2, 1, 1);
				}

				{
					Label label = new Label("Symbol");
					grid.add(label, 0, 3, 1, 1);

					TextField field = new TextField();
					Bindings.bindBidirectional(field.textProperty(), symbolProperty);
					grid.add(field, 1, 3, 1, 1);
				}

				{
					Label label = new Label("Description");
					grid.add(label, 0, 4, 1, 1);

					TextArea field = new TextArea();
					field.setPrefHeight(100);
					field.setPrefWidth(200);
					Bindings.bindBidirectional(field.textProperty(), descriptionProperty);
					grid.add(field, 1, 4, 1, 1);
				}

				{
					Label label = new Label("Category");
					grid.add(label, 0, 5, 1, 1);

					ChoiceBox<GamakaCategory> choiceBox = new ChoiceBox<>();
					choiceBox.setConverter(new StringConverter<GamakaCategory>() {

						@Override
						public String toString(GamakaCategory category) {
							return category.toString();
						}

						@Override
						public GamakaCategory fromString(String value) {
							return GamakaCategory.valueOf(value);
						}
					});
					choiceBox.getItems().addAll(GamakaCategory.values());
					Bindings.bindBidirectional(choiceBox.valueProperty(), categoryProperty);
					grid.add(choiceBox, 1, 5, 1, 1);
					choiceBox.setPrefWidth(200);
				}
				
				{
					Label label = new Label("Scale");
					grid.add(label, 0, 6, 1, 1);

					ChoiceBox<GamakaScale> choiceBox = new ChoiceBox<>();
					choiceBox.setConverter(new StringConverter<GamakaScale>() {

						@Override
						public String toString(GamakaScale scale) {
							return scale.toString();
						}

						@Override
						public GamakaScale fromString(String value) {
							return GamakaScale.valueOf(value);
						}
					});
					choiceBox.getItems().addAll(GamakaScale.values());
					Bindings.bindBidirectional(choiceBox.valueProperty(), scaleProperty);
					grid.add(choiceBox, 1, 6, 1, 1);
					choiceBox.setPrefWidth(200);
				}

				{
					Label label = new Label("Pitch Points");
					grid.add(label, 0, 7, 1, 1);

					TextArea field = new TextArea();
					field.setPrefHeight(300);
					field.setPrefWidth(200);
					Bindings.bindBidirectional(field.textProperty(), pitchPointsProperty);
					grid.add(field, 1, 7, 1, 1);
				}
			}

			{
				NumberAxis xAxis = new NumberAxis();
				xAxis.setLowerBound(0);
				xAxis.setUpperBound(127);
				xAxis.setTickUnit(16);
				xAxis.setMinorTickCount(16);
				xAxis.setAutoRanging(false);

				NumberAxis yAxis = new NumberAxis();
				yAxis.setLowerBound(-Gamaka.RANGE);
				yAxis.setUpperBound(Gamaka.RANGE);
				yAxis.setTickUnit(1);
				yAxis.setMinorTickCount(5);
				yAxis.setAutoRanging(false);

				ScatterChart<Number, Number> chart = new ScatterChart<Number, Number>(xAxis, yAxis);
				chart.setPrefWidth(800);
				chart.setPrefHeight(600);
				chart.setAnimated(false);

				XYChart.Series<Number, Number> noteBendSeries = new XYChart.Series<Number, Number>();
				chart.getData().add(noteBendSeries);

				XYChart.Series<Number, Number> pitchPointSeries = new XYChart.Series<Number, Number>();
				chart.getData().add(pitchPointSeries);

				chart.setOnMouseMoved(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {

						PitchPoint mousePoint = getPitchPointAtMouse(xAxis, yAxis, event);

						int tick = mousePoint.getTick();
						double noteBend = mousePoint.getNoteBend();
						pitchPointSeries.setName("" + tick + "," + noteBend);
					}
				});

				chart.setOnMouseClicked(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						PitchPoint mousePoint = getPitchPointAtMouse(xAxis, yAxis, event);
						int mouseTick = mousePoint.getTick();

						List<PitchPoint> currentPoints = convertToList(pitchPointsProperty.get());
						int insertAt = currentPoints.size();
						for (int i = 0; i < currentPoints.size(); i++) {
							PitchPoint point = currentPoints.get(i);
							int pointTick = point.getTick();
							if (pointTick == mouseTick) {
								currentPoints.remove(i);
								insertAt = i;
								break;
							}
							if (pointTick > mouseTick) {
								insertAt = i;
								break;
							}
						}
						currentPoints.add(insertAt, mousePoint);
						pitchPointsProperty.set(convertToCsv(currentPoints));
					}
				});

				pitchPointsProperty.addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue,
							String newValue) {

						ObservableList<Data<Number, Number>> pitchPointsData = pitchPointSeries.getData();
						pitchPointsData.clear();

						List<PitchPoint> pitchPoints = convertToList(newValue);
						for (PitchPoint pitchPoint : pitchPoints) {
							int tick = pitchPoint.getTick();
							double noteBend = pitchPoint.getNoteBend();
							pitchPointsData.add(new Data<Number, Number>(tick, noteBend));
						}

						ObservableList<Data<Number, Number>> noteBendData = noteBendSeries.getData();
						noteBendData.clear();

						double[] noteBends = Gamaka.calculateNoteBends(pitchPoints);
						for (int i = 0; i < noteBends.length; i++) {
							double noteBend = noteBends[i];
							noteBendData.add(new Data<Number, Number>(i, noteBend));
						}
					}
				});

				hbox.getChildren().add(chart);
			}
		}

		{
			TextArea message = new TextArea();
			message.setPadding(PADDING_5);
			message.setPrefWidth(200);
			message.setPrefHeight(50);
			message.setWrapText(true);
			message.setEditable(false);
			message.getStyleClass().add("message");
			message.textProperty().bind(messageProperty);
			root.getChildren().add(message);
		}

		Scene scene = new Scene(root);
		scene.getStylesheets().add("/net/stmdhr/gamidi/application.css");
		stage.setScene(scene);

		stage.setTitle("Gamaka Editor");
		stage.setMaximized(true);
		stage.show();

	}

	public String convertToCsv(List<PitchPoint> pitchPoints) {
		StringBuilder builder = new StringBuilder();
		for (PitchPoint point : pitchPoints) {
			builder.append(point.getTick());
			builder.append(",");
			builder.append(point.getNoteBend());
			builder.append("\n");
		}
		return builder.toString();
	}

	public List<PitchPoint> convertToList(String csv) {
		List<PitchPoint> pitchPoints = new ArrayList<>();
		try {
			if (StringUtils.isNotEmpty(csv)) {
				LineIterator iterator = new LineIterator(new StringReader(csv));
				while (iterator.hasNext()) {
					String line = iterator.next();
					String[] split = line.split(",");

					int tick = Integer.valueOf(split[0]);
					double semitoneBend = Double.valueOf(split[1]);
					PitchPoint pitchPoint = new PitchPoint(tick, semitoneBend);

					pitchPoints.add(pitchPoint);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error setting pitch points for csv={}", csv, e);
		}
		return pitchPoints;
	}

	@Override
	public void stop() {

		LOGGER.debug("Stopping");
		player.shutdown();
	}

	private PitchPoint getPitchPointAtMouse(NumberAxis xAxis, NumberAxis yAxis, MouseEvent event) {
		Point2D pointInScene = new Point2D(event.getSceneX(), event.getSceneY());
		double xPosInAxis = xAxis.sceneToLocal(new Point2D(pointInScene.getX(), 0)).getX();
		double yPosInAxis = yAxis.sceneToLocal(new Point2D(0, pointInScene.getY())).getY();
		Number xVal = xAxis.getValueForDisplay(xPosInAxis);
		Number yVal = yAxis.getValueForDisplay(yPosInAxis);
		int tick = xVal.intValue();
		double noteBend = Math.round(yVal.doubleValue() * 10) / 10.0;
		PitchPoint pitchPoint = new PitchPoint(tick, noteBend);
		return pitchPoint;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
