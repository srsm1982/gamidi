package net.stmdhr.gamidi.swara.gamaka;

public enum GamakaCategory {

	NONE, // 
	ROHANAM, // Proceeding from the given note - to up or to down.
	AGAMANAM, // Proceeding to the given note - from up or from down.
	AAHATA, // Oscillating once above or below.
	KAMPITHA, // Oscillating multiple times above or below.
	ANDOLAM, // Meandering across multiple notes.
	;

}
