package net.stmdhr.gamidi.swara.gamaka.reporter;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;
import net.stmdhr.gamidi.swara.gamaka.GamakaShapeUtil;

public class GamakaReporter {

	public static void main(String[] args) {
		
		try {
			Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
			cfg.setClassForTemplateLoading(GamakaReporter.class, "");
			cfg.setIncompatibleImprovements(new Version(2, 3, 20));
			cfg.setDefaultEncoding("UTF-8");
			cfg.setLocale(Locale.US);
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

			Map<String, Object> input = new HashMap<String, Object>();
			input.put("GamakaShapeUtil", new GamakaShapeUtil());
			input.put("gamakas", Gamaka.values());

			Template template = cfg.getTemplate("template.html");

	        Writer fileWriter = new FileWriter(new File("gamaka-report.html"));
	        try {
	            template.process(input, fileWriter);
	        } finally {
	            fileWriter.close();
	        }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
