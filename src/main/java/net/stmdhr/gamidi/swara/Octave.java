package net.stmdhr.gamidi.swara;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

public enum Octave {

	DOWN1('\u0323', -1), // One dot below
	BASE('\u034f', 0), // No glyph
	UP1('\u0307', +1), // One dot above
	;
	
	public static final Octave DEFAULT = BASE;
	
	private final char symbol;
	private final Integer value;

	Octave(char symbol, Integer value) {
		this.symbol = symbol;
		this.value = value;
	}

	public char getSymbol() {
		return symbol;
	}

	@JsonValue
	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return this.name();
	}

	public static Octave fromSymbol(char symbol) {
		Octave[] values = values();
		for (Octave octave : values) {
			if (octave.symbol == symbol) {
				return octave;
			}
		}
		return BASE;
	}

	@JsonCreator
	public static Octave fromValue(int value) {
		Octave[] values = values();
		for (Octave octave : values) {
			if (octave.value == value) {
				return octave;
			}
		}
		return BASE;
	}

	public Octave getNextOctave() {
		int currentValue = getValue();
		int newValue = (currentValue == 1) ? currentValue : currentValue + 1;
		return Octave.fromValue(newValue);
	}
	
	public Octave getPreviousOctave() {
		int currentValue = getValue();
		int newValue = (currentValue == -1) ? currentValue : currentValue - 1;
		return Octave.fromValue(newValue);
	}
}