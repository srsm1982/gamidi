package net.stmdhr.gamidi.swara;

import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Shruti;
import net.stmdhr.gamidi.ShrutiOctave;
import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;

public class Swara {

	public static final byte[] SWARA_COMPLETED = "sc".getBytes();

	private static final Logger LOGGER = LoggerFactory.getLogger(Gamaka.class);

	private boolean isTrailer;
	private Note note;
	private Octave octave;
	private Gamaka gamaka;
	private Duration duration;

	public Swara() {
	}

	public static Swara createTrailer() {
		Swara swara = new Swara();
		swara.isTrailer = true;
		return swara;
	}

	public static Swara create(Note note, Octave octave, Gamaka gamaka, Duration duration) {
		Swara swara = new Swara();
		swara.note = note;
		swara.octave = octave;
		swara.gamaka = gamaka;
		swara.duration = duration;
		swara.isTrailer = false;
		return swara;
	}

	public long addMidiEvents(long startTick, Track track) {

		try {

			// Duration
			double durationValue = duration.getValue();
			long endTick = startTick + (long) (durationValue * Player.PLAYER_RESOLUTION);

			if (!Note.REST.equals(note)) {

				// Note
				int noteValue = note.getValue();
				int octaveValue = octave.getValue();

				// Raaga
				Raaga raaga = Song.getInstance().getRaaga();
				int[] raagaSemitones = raaga.getSemitones();

				// Shruti Octave
				ShrutiOctave shurtiOctave = Song.getInstance().getShrutiOctave();
				int shurtiOctaveValue = shurtiOctave.getValue();

				// Shruti
				Shruti shruti = Song.getInstance().getShruti();
				int shrutiValue = shruti.getValue();

				// Key
				int key = (shurtiOctaveValue * 12) + shrutiValue + (octaveValue * 12) + raagaSemitones[noteValue];

				// Note on
				ShortMessage onMessage = new ShortMessage();
				onMessage.setMessage(ShortMessage.NOTE_ON, 0, key, 127);
				MidiEvent onEvent = new MidiEvent(onMessage, startTick);
				track.add(onEvent);

				// Gamaka
				gamaka.addMidiEvents(track, note, raaga, duration, startTick);

				// Note off
				ShortMessage offMessage = new ShortMessage();
				offMessage.setMessage(ShortMessage.NOTE_OFF, 0, key, 127);
				MidiEvent offEvent = new MidiEvent(offMessage, endTick - 1);
				track.add(offEvent);

			}

			// Swara completed
			MetaMessage metaMessage = new MetaMessage(0x07, SWARA_COMPLETED, SWARA_COMPLETED.length);
			MidiEvent metaEvent = new MidiEvent(metaMessage, endTick - 1);
			track.add(metaEvent);

			return endTick;

		} catch (Exception e) {
			LOGGER.error("Error creating midi events for swara={}", this, e);
			return startTick;
		}

	}

	@Override
	public String toString() {
		return note + "," + octave + "," + gamaka + "," + duration;
	}

	public Note getNote() {
		return note;
	}

	public Octave getOctave() {
		return octave;
	}

	public Gamaka getGamaka() {
		return gamaka;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setNote(Note note) {
		this.note = note;
	}

	public void setOctave(Octave octave) {
		this.octave = octave;
	}

	public void setGamaka(Gamaka gamaka) {
		this.gamaka = gamaka;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public boolean isTrailer() {
		return isTrailer;
	}

	public void setTrailer(boolean isTrailer) {
		this.isTrailer = isTrailer;
	}

	@Override
	public Swara clone() {
		Swara clone = new Swara();
		clone.isTrailer = isTrailer;
		clone.note = note;
		clone.duration = duration;
		clone.octave = octave;
		clone.gamaka = gamaka;
		return clone;
	}

}
