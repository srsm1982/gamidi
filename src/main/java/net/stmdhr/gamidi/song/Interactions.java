package net.stmdhr.gamidi.song;

import javax.sound.midi.Instrument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Shruti;
import net.stmdhr.gamidi.ShrutiOctave;
import net.stmdhr.gamidi.controls.DroneVolumeSelector;
import net.stmdhr.gamidi.controls.DurationSelector;
import net.stmdhr.gamidi.controls.GamakaSelector;
import net.stmdhr.gamidi.controls.InstrumentSelector;
import net.stmdhr.gamidi.controls.OctaveSelector;
import net.stmdhr.gamidi.controls.RaagaSelector;
import net.stmdhr.gamidi.controls.ShrutiSelector;
import net.stmdhr.gamidi.controls.SwaraTextArea;
import net.stmdhr.gamidi.controls.TempoSelector;
import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.Duration;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.Octave;
import net.stmdhr.gamidi.swara.Swara;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;

public class Interactions {

	private static final Logger LOGGER = LoggerFactory.getLogger(Interactions.class);

	public static void init() {

		initRaaga();

		initInstrument();

		initDroneVolume();

		initTempo();

		initShrutiOctave();

		initShruti();

		initOctave();

		initDuration();
		
		initGamaka();

		initActiveSwara();

	}

	private static void initActiveSwara() {

		SwaraTextArea swaraTextAreaView = SwaraTextArea.getInstance();
		OctaveSelector octaveView = OctaveSelector.getInstance();
		DurationSelector durationView = DurationSelector.getInstance();
		GamakaSelector gamakaView = GamakaSelector.getInstance();

		swaraTextAreaView.addActiveSwaraListener(new ChangeListener<Swara>() {
			@Override
			public void changed(ObservableValue<? extends Swara> observable, Swara oldValue, Swara newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {

					LOGGER.trace("swaraTextAreaView >> octaveView");
					if (newValue.isTrailer()) {

						octaveView.setEnabled(false);
						durationView.setEnabled(false);
						gamakaView.setEnabled(false);

					} else {

						Note note = newValue.getNote();
						Duration duration = newValue.getDuration();
						Octave octave = newValue.getOctave();
						Gamaka gamaka = newValue.getGamaka();

						if (Note.REST.equals(note)) {

							durationView.setEnabled(true);
							durationView.setValue(duration);

							octaveView.setEnabled(false);
							gamakaView.setEnabled(false);

						} else {

							octaveView.setEnabled(true);
							octaveView.setValue(octave);

							durationView.setEnabled(true);
							durationView.setValue(duration);

							gamakaView.setEnabled(true);
							gamakaView.setValue(gamaka);
						}
					}
				}
			}
		});
	}
	
	private static void initOctave() {

		OctaveSelector octaveView = OctaveSelector.getInstance();
		SwaraTextArea swaraTextAreaView = SwaraTextArea.getInstance();

		octaveView.addListener(new ChangeListener<Octave>() {
			@Override
			public void changed(ObservableValue<? extends Octave> observable, Octave oldValue, Octave newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("octaveView >> swaraTextAreaView");
					swaraTextAreaView.setActiveSwaraOctave(newValue);
				}
			}
		});
	}

	private static void initDuration() {

		DurationSelector durationView = DurationSelector.getInstance();
		SwaraTextArea swaraTextAreaView = SwaraTextArea.getInstance();

		durationView.addListener(new ChangeListener<Duration>() {
			@Override
			public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("durationView >> swaraTextAreaView");
					swaraTextAreaView.setActiveSwaraDuration(newValue);
				}
			}
		});
	}
	
	private static void initGamaka() {
		
		GamakaSelector gamakaView = GamakaSelector.getInstance();
		SwaraTextArea swaraTextAreaView = SwaraTextArea.getInstance();

		gamakaView.addListener(new ChangeListener<Gamaka>() {
			@Override
			public void changed(ObservableValue<? extends Gamaka> observable, Gamaka oldValue, Gamaka newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("gamakaView >> swaraTextAreaView");
					swaraTextAreaView.setActiveSwaraGamaka(newValue);
				}
			}
		});
	}

	private static void initShruti() {

		ObjectProperty<Shruti> shrutiModel = Song.getInstance().getShrutiProperty();
		ShrutiSelector shrutiView = ShrutiSelector.getInstance();

		shrutiView.addShrutiListener(new ChangeListener<Shruti>() {
			@Override
			public void changed(ObservableValue<? extends Shruti> observable, Shruti oldValue, Shruti newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("shrutiView >> shrutiModel");
					shrutiModel.set(newValue);
				}
			}
		});

		shrutiModel.addListener(new ChangeListener<Shruti>() {
			@Override
			public void changed(ObservableValue<? extends Shruti> observable, Shruti oldValue, Shruti newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("shrutiModel >> shrutiView");
					shrutiView.setShrutiValue(newValue);
				}
			}
		});

		// Set default
		shrutiModel.set(Shruti.DEFAULT);
	}

	private static void initShrutiOctave() {

		ObjectProperty<ShrutiOctave> shrutiOctaveModel = Song.getInstance().getShrutiOctaveProperty();
		ShrutiSelector shrutiOctaveView = ShrutiSelector.getInstance();

		shrutiOctaveView.addShrutiOctaveListener(new ChangeListener<ShrutiOctave>() {
			@Override
			public void changed(ObservableValue<? extends ShrutiOctave> observable, ShrutiOctave oldValue,
					ShrutiOctave newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("shrutiOctaveView >> shrutiOctaveModel");
					shrutiOctaveModel.set(newValue);
				}
			}
		});

		shrutiOctaveModel.addListener(new ChangeListener<ShrutiOctave>() {
			@Override
			public void changed(ObservableValue<? extends ShrutiOctave> observable, ShrutiOctave oldValue,
					ShrutiOctave newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("shrutiOctaveModel >> shrutiOctaveView");
					shrutiOctaveView.setShrutiOctaveValue(newValue);
				}
			}
		});

		// Set default
		shrutiOctaveModel.set(ShrutiOctave.DEFAULT);
	}

	private static void initTempo() {

		Player player = Player.getInstance();
		IntegerProperty tempoModel = Song.getInstance().getTempoProperty();
		TempoSelector tempoView = TempoSelector.getInstance();

		tempoModel.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null) {
					LOGGER.trace("tempoModel >> player");
					player.setTempo(newValue.intValue());
				}
			}
		});

		tempoView.addListener(new ChangeListener<Integer>() {
			@Override
			public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("tempoView >> tempoModel");
					tempoModel.set(newValue);
				}
			}
		});

		tempoModel.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("tempoModel >> tempoView");
					tempoView.setValue(newValue.intValue());
				}
			}
		});

		// Set default
		tempoModel.set(TempoSelector.DEFAULT_VALUE);
	}

	private static void initDroneVolume() {

		Player player = Player.getInstance();
		IntegerProperty droneVolumeModel = Song.getInstance().getDroneVolumeProperty();
		DroneVolumeSelector droneVolumeView = DroneVolumeSelector.getInstance();

		droneVolumeModel.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null) {
					LOGGER.trace("droneVolumeModel >> player");
					player.setDroneVolume(newValue.intValue());
				}
			}
		});

		droneVolumeView.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("droneVolumeView >> droneVolumeModel");
					droneVolumeModel.set(newValue.intValue());
				}
			}
		});

		droneVolumeModel.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("droneVolumeModel >> droneVolumeView");
					droneVolumeView.setValue(newValue.intValue());
				}
			}
		});

		// Set default
		droneVolumeModel.set(DroneVolumeSelector.DEFAULT_VALUE);
	}

	private static void initInstrument() {

		ObjectProperty<Instrument> instrumentModel = Song.getInstance().getInstrumentProperty();
		InstrumentSelector instrumentView = InstrumentSelector.getInstance();
		Player player = Player.getInstance();

		instrumentModel.addListener(new ChangeListener<Instrument>() {
			@Override
			public void changed(ObservableValue<? extends Instrument> observable, Instrument oldValue,
					Instrument newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("instrumentModel >> player");
					player.setInstrument(newValue);
				}
			}
		});

		instrumentView.addListener(new ChangeListener<Instrument>() {
			@Override
			public void changed(ObservableValue<? extends Instrument> observable, Instrument oldValue,
					Instrument newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("instrumentView >> instrumentModel");
					instrumentModel.set(newValue);
				}
			}
		});

		instrumentModel.addListener(new ChangeListener<Instrument>() {
			@Override
			public void changed(ObservableValue<? extends Instrument> observable, Instrument oldValue,
					Instrument newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("instrumentModel >> instrumentView");
					instrumentView.setValue(newValue);
				}
			}
		});

		// Set default
		instrumentModel.set(player.getDefaultInstrument());
	}

	private static void initRaaga() {

		ObjectProperty<Raaga> raagaModel = Song.getInstance().getRaagaProperty();
		RaagaSelector raagaView = RaagaSelector.getInstance();

		raagaView.addListener(new ChangeListener<Raaga>() {
			@Override
			public void changed(ObservableValue<? extends Raaga> observable, Raaga oldValue, Raaga newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("raagaView >> raagaModel");
					raagaModel.set(newValue);
				}
			}
		});

		raagaModel.addListener(new ChangeListener<Raaga>() {
			@Override
			public void changed(ObservableValue<? extends Raaga> observable, Raaga oldValue, Raaga newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("raagaModel >> raagaView");
					raagaView.setValue(newValue);
				}
			}
		});

		// Set default
		raagaModel.set(Raaga.DEFAULT);
	}

}
