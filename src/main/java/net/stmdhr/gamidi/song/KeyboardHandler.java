package net.stmdhr.gamidi.song;

import javafx.event.EventHandler;
import javafx.scene.control.Toggle;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import net.stmdhr.gamidi.controls.DurationSelector;
import net.stmdhr.gamidi.controls.OctaveSelector;
import net.stmdhr.gamidi.controls.SwaraTextArea;
import net.stmdhr.gamidi.swara.Duration;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.Octave;
import net.stmdhr.gamidi.swara.Swara;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;

public class KeyboardHandler {

	private SwaraTextArea swaraTextArea = SwaraTextArea.getInstance();

	private static final KeyboardHandler INSTANCE = new KeyboardHandler();

	private KeyboardHandler() {
	}

	public static KeyboardHandler getInstance() {
		return INSTANCE;
	}

	public EventHandler<KeyEvent> getKeyPressedHandler() {
		return keyPressedHandler;
	}

	public EventHandler<KeyEvent> getKeyTypedHandler() {
		return keyTypedHandler;
	}

	private EventHandler<KeyEvent> keyPressedHandler = new EventHandler<KeyEvent>() {

		@Override
		public void handle(KeyEvent event) {

			KeyCode code = event.getCode();

			boolean isShiftDown = event.isShiftDown();
			boolean isControlDown = event.isControlDown();

			switch (code) {

				case DELETE: {
					swaraTextArea.deleteSelection();
					event.consume();
					break;
				}
				case LEFT: {
					swaraTextArea.moveLeft(!isShiftDown);
					event.consume();
					break;
				}
				case RIGHT: {
					swaraTextArea.moveRight(!isShiftDown);
					event.consume();
					break;
				}
				case UP: {
					swaraTextArea.moveUp(!isShiftDown);
					event.consume();
					break;
				}
				case DOWN: {
					swaraTextArea.moveDown(!isShiftDown);
					event.consume();
					break;
				}
				case C: {
					if (isControlDown) {
						swaraTextArea.copyToClipboard();
					}
					event.consume();
					break;
				}
				case V: {
					if (isControlDown) {
						swaraTextArea.pasteFromClipboard();
					}
					event.consume();
					break;
				}
				case A: {
					if (isControlDown) {
						swaraTextArea.selectAll();
					}
					event.consume();
					break;
				}
				case OPEN_BRACKET: {
					Duration current = DurationSelector.getInstance().getValue();
					Duration previous = current.getPreviousDuration();
					DurationSelector.getInstance().setValue(previous);
					event.consume();
					break;
				}
				case CLOSE_BRACKET: {
					Duration current = DurationSelector.getInstance().getValue();
					Duration next = current.getNextDuration();
					DurationSelector.getInstance().setValue(next);
					event.consume();
					break;
				}
				case MINUS: {
					Octave current = OctaveSelector.getInstance().getValue();
					Octave previous = current.getPreviousOctave();
					OctaveSelector.getInstance().setValue(previous);
					event.consume();
					break;
				}
				case EQUALS: {
					Octave current = OctaveSelector.getInstance().getValue();
					Octave next = current.getNextOctave();
					OctaveSelector.getInstance().setValue(next);
					event.consume();
					break;
				}

				case HOME: {
					swaraTextArea.moveToFirst();
					event.consume();
					break;
				}

				case END: {
					swaraTextArea.moveToLast();
					event.consume();
					break;
				}
				default:
					break;

			}
		}

	};

	private EventHandler<KeyEvent> keyTypedHandler = new EventHandler<KeyEvent>() {

		@Override
		public void handle(KeyEvent event) {
			String character = event.getCharacter();
			switch (character) {
				case "\b": {
					swaraTextArea.backspace();
					event.consume();
					break;
				}

				case "\r":
				case "\n": {
					swaraTextArea.addNewLine();
					event.consume();
					break;
				}

				case "\t": {
					// Tab out
					break;
				}

				case "s":
				case "r":
				case "g":
				case "m":
				case "p":
				case "d":
				case "n": {
					Note note = Note.fromSymbol(character.charAt(0));
					Octave octave = Octave.BASE;
					Duration duration = Duration.FULL;
					Gamaka gamaka = Gamaka.NO_GAMAKA;

					Swara swara = Swara.create(note, octave, gamaka, duration);
					swaraTextArea.addSwara(swara);

					event.consume();
					break;
				}

				case "S":
				case "R":
				case "G":
				case "M":
				case "P":
				case "D":
				case "N": {
					Note note = Note.fromSymbol(character.charAt(0));
					Octave octave = Octave.UP1;
					Duration duration = Duration.FULL;
					Gamaka gamaka = Gamaka.NO_GAMAKA;

					Swara swara = Swara.create(note, octave, gamaka, duration);
					swaraTextArea.addSwara(swara);

					event.consume();
					break;
				}

				case ",":
				case ".":
					Note note = Note.REST;
					Duration duration = Duration.FULL;

					Swara swara = Swara.create(note, null, null, duration);
					swaraTextArea.addSwara(swara);

					event.consume();
					break;

				case " ":

				default:
					break;
			}
		}
	};

}
