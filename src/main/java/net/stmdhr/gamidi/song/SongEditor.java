package net.stmdhr.gamidi.song;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Settings;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.controls.DroneVolumeSelector;
import net.stmdhr.gamidi.controls.DurationSelector;
import net.stmdhr.gamidi.controls.GamakaSelector;
import net.stmdhr.gamidi.controls.InstrumentSelector;
import net.stmdhr.gamidi.controls.OctaveSelector;
import net.stmdhr.gamidi.controls.RaagaSelector;
import net.stmdhr.gamidi.controls.ShrutiSelector;
import net.stmdhr.gamidi.controls.SwaraTextArea;
import net.stmdhr.gamidi.controls.TempoSelector;
import net.stmdhr.gamidi.data.SongIO;
import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.Swara;

public class SongEditor extends Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(SongEditor.class);

	private final Player player = Player.getInstance();

	private final EventHandler<KeyEvent> keyPressedHandler = KeyboardHandler.getInstance().getKeyPressedHandler();
	private final EventHandler<KeyEvent> keyTypedHandler = KeyboardHandler.getInstance().getKeyTypedHandler();

	private Label message;

	static {

		Interactions.init();

		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				LOGGER.error("Uncaught exception", e);
				e.printStackTrace();
			}
		});

	}

	@Override
	public void stop() {
		LOGGER.debug("Stopping");
		player.shutdown();
	}

	@Override
	public void start(Stage stage) {

		VBox root = new VBox();
		root.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedHandler);
		root.addEventFilter(KeyEvent.KEY_TYPED, keyTypedHandler);

		{
			MenuBar menuBar = createMenuBar(stage);
			root.getChildren().add(menuBar);

			{
				HBox row = new HBox(5);
				root.getChildren().add(row);

				Node settingsButton = createSettingsButton(stage);
				row.getChildren().add(settingsButton);
			}

			root.getChildren().add(createHorizontalSeparator());

			{
				HBox row = new HBox();
				root.getChildren().add(row);

				// Octave
				Node octaveSelector = OctaveSelector.getInstance().getNode();
				row.getChildren().add(octaveSelector);

				// Separator
				row.getChildren().add(createVerticalSeparator());

				// Duration
				Node durationSelector = DurationSelector.getInstance().getNode();
				row.getChildren().add(durationSelector);

				// Separator
				row.getChildren().add(createVerticalSeparator());

				// Play
				HBox playLeadButton = createPlayLeadButton();
				row.getChildren().add(playLeadButton);

				// Separator
				row.getChildren().add(createVerticalSeparator());

				// Play Drone
				HBox playDroneButton = createPlayDroneButton();
				row.getChildren().add(playDroneButton);
			}

			{
				HBox row = new HBox();
				VBox.setVgrow(row, Priority.ALWAYS);
				root.getChildren().add(row);

				Node swaraTextArea = SwaraTextArea.getInstance().getNode();
				row.getChildren().add(swaraTextArea);
				VBox.setVgrow(swaraTextArea, Priority.ALWAYS);
				HBox.setHgrow(swaraTextArea, Priority.ALWAYS);

				Node gamakaSelector = GamakaSelector.getInstance().getNode();
				row.getChildren().add(gamakaSelector);
			}

			{
				HBox row = new HBox();
				root.getChildren().add(row);

				Node messageArea = createMessageArea();
				row.getChildren().add(messageArea);
				HBox.setHgrow(messageArea, Priority.ALWAYS);
			}

		}

		stage.setOnCloseRequest(confirmCloseEventHandler);

		// TODO Unused?
		Button closeButton = new Button("Close Application");
		closeButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
			}
		});

		Scene scene = new Scene(root);
		// scene.getStylesheets().add("/net/stmdhr/gamidi/bootstrap3.css");
		scene.getStylesheets().add("/net/stmdhr/gamidi/application.css");
		stage.setScene(scene);

		stage.setTitle("Gamidi");
		stage.setMaximized(true);
		stage.show();
	}

	private Node createSettingsButton(Stage stage) {

		HBox row = new HBox();
		row.setPadding(Style.PADDING_5);
		row.setAlignment(Pos.CENTER_LEFT);

		Stage dialog = createSettingsDialog(stage);
		Button button = new Button("Settings");
		button.setPadding(Style.PADDING_5);
		button.setAlignment(Pos.CENTER_LEFT);
		button.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				dialog.showAndWait();
			}
		});
		row.getChildren().add(button);
		

		row.getChildren().add(createVerticalSeparator());

		Label settingsLabel = new Label();
		settingsLabel.setText("Settings: ");
		settingsLabel.setAlignment(Pos.CENTER_LEFT);
		row.getChildren().add(settingsLabel);

		return row;
	}

	private EventHandler<WindowEvent> confirmCloseEventHandler = event -> {

		Alert closeConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
		closeConfirmation.setContentText("Are you sure you want to exit?");

		Button exitButton = (Button) closeConfirmation.getDialogPane().lookupButton(ButtonType.OK);
		exitButton.setText("Exit");
		closeConfirmation.setHeaderText("Confirm Exit");

		Optional<ButtonType> closeResponse = closeConfirmation.showAndWait();
		if (!ButtonType.OK.equals(closeResponse.get())) {
			event.consume();
		}
	};

	private Separator createHorizontalSeparator() {
		return new Separator(Orientation.HORIZONTAL);
	}

	private Separator createVerticalSeparator() {
		return new Separator(Orientation.VERTICAL);
	}

	private TitledPane createMessageArea() {
		TitledPane pane = new TitledPane();
		pane.setText("Info");
		pane.setPadding(Style.PADDING_5);
		pane.setCollapsible(false);

		HBox hbox = new HBox();
		pane.setContent(hbox);

		message = new Label();
		message.getStyleClass().add("message");
		message.setText("Hi..!");
		message.setAlignment(Pos.TOP_LEFT);
		message.setTextAlignment(TextAlignment.LEFT);
		hbox.getChildren().add(message);
		return pane;
	}

	private HBox createPlayDroneButton() {
		HBox hbox = new HBox();
		hbox.setPadding(Style.PADDING_5);

		ToggleButton play = new ToggleButton("Play Drone");
		play.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(play, Priority.ALWAYS);
		hbox.getChildren().add(play);

		play.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					play.setText("Pause Drone");
					player.startDrone();
				} else {
					play.setText("Play Drone");
					player.stopDrone();
				}
			}
		});
		return hbox;
	}

	private Label createDroneVolumeLabel() {
		Label label = new Label("Drone Volume");
		label.setContentDisplay(ContentDisplay.LEFT);
		label.setPadding(Style.PADDING_5);
		return label;
	}

	private HBox createPlayLeadButton() {
		HBox hbox = new HBox();
		hbox.setPadding(Style.PADDING_5);

		ToggleButton play = new ToggleButton("Play");
		play.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(play, Priority.ALWAYS);
		hbox.getChildren().add(play);

		play.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					play.setText("Pause");
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							// Play all swaras from caret
							SwaraTextArea.getInstance().playFromCaret();
						}
					});
				} else {
					SwaraTextArea.getInstance().stopPlaying();
					play.setText("Play ");
				}
			}
		});

		player.addLeadEventListener(new MetaEventListener() {
			@Override
			public void meta(MetaMessage metaMsg) {
				if (metaMsg.getType() == Player.END_OF_TRACK_MESSAGE_TYPE) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							play.setSelected(false);
						}
					});
				}
			}
		});
		return hbox;
	}

	private Label createTempoLabel() {
		Label label = new Label("Tempo");
		label.setContentDisplay(ContentDisplay.LEFT);
		label.setPadding(Style.PADDING_5);
		return label;
	}

	private Label createShrutiLabel() {
		Label label = new Label("Scale & Shruti");
		label.setContentDisplay(ContentDisplay.LEFT);
		label.setPadding(Style.PADDING_5);
		return label;
	}

	private Stage createSettingsDialog(Stage primaryStage) {

		VBox root = new VBox();
		root.setPrefSize(300, 500);

		Node raagaSelector = RaagaSelector.getInstance().getNode();
		root.getChildren().add(raagaSelector);

		Node instrumentSelector = InstrumentSelector.getInstance().getNode();
		root.getChildren().add(instrumentSelector);

		Label shrutiLabel = createShrutiLabel();
		root.getChildren().add(shrutiLabel);

		Node shrutiControl = ShrutiSelector.getInstance().getNode();
		root.getChildren().add(shrutiControl);

		Label tempoLabel = createTempoLabel();
		root.getChildren().add(tempoLabel);

		Node tempoControl = TempoSelector.getInstance().getNode();
		root.getChildren().add(tempoControl);

		Label droneVolumeLabel = createDroneVolumeLabel();
		root.getChildren().add(droneVolumeLabel);

		Node droneVolumeControl = DroneVolumeSelector.getInstance().getNode();
		root.getChildren().add(droneVolumeControl);

		// Create scene
		ScrollPane pane = new ScrollPane(root);
		Scene scene = new Scene(pane);

		// Create stage
		Stage dialogStage = new Stage();
		dialogStage.setScene(scene);
		dialogStage.setResizable(false);
		dialogStage.centerOnScreen();
		dialogStage.initOwner(primaryStage);
		dialogStage.initModality(Modality.APPLICATION_MODAL);

		return dialogStage;
	}

	private MenuBar createMenuBar(Stage stage) {
		MenuBar bar = new MenuBar();

		Menu file = new Menu("File");
		bar.getMenus().add(file);

		MenuItem open = new MenuItem("Open");
		open.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					FileChooser chooser = new FileChooser();
					chooser.setTitle("Select Gamaka file");

					String userHome = Settings.getSongsDirectory();
					chooser.setInitialDirectory(new File(userHome));
					chooser.getExtensionFilters().add(Settings.SONG_FILTER);

					File file = chooser.showOpenDialog(stage);
					if (file != null) {
						String content = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
						SongIO.readSong(content);
						SwaraTextArea.getInstance().readAll(Song.getInstance().getSwaras());
					}

				} catch (Exception e) {

					LOGGER.error("Error loading file", e);
				}
			}
		});
		file.getItems().add(open);

		MenuItem save = new MenuItem("Save");
		save.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					FileChooser chooser = new FileChooser();
					chooser.setTitle("Save Song");

					String userHome = Settings.getSongsDirectory();
					chooser.setInitialDirectory(new File(userHome));
					chooser.getExtensionFilters().add(Settings.SONG_FILTER);

					File file = chooser.showSaveDialog(stage);

					List<Swara> swaras = SwaraTextArea.getInstance().getAllSwaras();

					Song song = Song.getInstance();
					song.setSwaras(swaras);

					String content = SongIO.write(song);
					FileUtils.write(file, content, Charset.forName("UTF-8"), false);
					message.setText("Successfully saved the file.");

				} catch (Exception e) {

					message.setText("Error saving the file.");
					LOGGER.error("Error writing file", e);
				}
			}
		});
		file.getItems().add(save);

		Menu help = new Menu("Help");
		bar.getMenus().add(help);
		return bar;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
