package net.stmdhr.gamidi.song;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Toggle;
import javafx.scene.layout.HBox;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.Swara;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;
import net.stmdhr.gamidi.swara.gamaka.GamakaButton;

public class GamakaGroup extends SelectableToggleGroup {

	private static final GamakaGroup INSTANCE = new GamakaGroup();

	public static GamakaGroup getInstance() {
		return INSTANCE;
	}

	private GamakaGroup() {
//		selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
//			@Override
//			public void changed(ObservableValue<? extends Toggle> ov, Toggle oldValue, Toggle newValue) {
//				if (newValue != null && !newValue.equals(oldValue)) {
//
//					Gamaka gamaka = (Gamaka) newValue.getUserData();
//					int id = gamaka.getId();
//					GamakaButton button = GamakaButton.getGamakaButton(id);
//
//					shapeBox.getChildren().clear();
//					shapeBox.getChildren().add(button.getShape());
//				}
//			}
//		});
	}

	private final HBox shapeBox = new HBox();
	private Note currentNote;

	public HBox getShapeBox() {
		return shapeBox;
	}

	public void updateSuggestions(Swara swara) {

		if (swara != null ) {
			if (!swara.isTrailer()) {
				if (!swara.getNote().equals(currentNote)) {

					currentNote = swara.getNote();

					Raaga raaga = Song.getInstance().getRaaga();
					Note note = swara.getNote();

					// List<Integer> suggestedGamakas = raaga.getSuggestedGamakas(note);
					// List<Integer> allGamakas = Gamaka.ids();
					// gamakasBox.clear();
					//
					// gamakasBox.add(GamakaButton.SUGGESTED_HEADING);
					// for (Integer id : suggestedGamakas) {
					// // First add suggested gamakas
					// ToggleButton button = GamakaButton.getButton(id);
					// gamakasBox.add(button);
					// }
					//
					// gamakasBox.add(GamakaButton.OTHER_HEADING);
					// for (Integer id : allGamakas) {
					// // Then add other gamakas
					// ToggleButton button = GamakaButton.getButton(id);
					// if (!suggestedGamakas.contains(id)) {
					// gamakasBox.add(button);
					// }
					// }
				}
			}
		}
	}
}
