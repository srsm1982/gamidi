package net.stmdhr.gamidi.song;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

public class SelectableToggleGroup extends ToggleGroup {

	private BooleanProperty enabledProperty = new SimpleBooleanProperty();
	private ObjectProperty<Object> valueProperty = new SimpleObjectProperty<>();

	public SelectableToggleGroup() {

		enabledProperty.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					ObservableList<Toggle> toggles = getToggles();
					for (Toggle toggle : toggles) {
						if (toggle instanceof ToggleButton) {
							ToggleButton button = (ToggleButton) toggle;
							button.setDisable(!newValue);
						}
					}
				}
			}
		});

		valueProperty.addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					ObservableList<Toggle> toggles = getToggles();
					for (Toggle toggle : toggles) {
						Object userData = toggle.getUserData();
						if (newValue.equals(userData)) {
							toggle.setSelected(true);
						} else {
							toggle.setSelected(false);
						}
					}
				}
			}
		});
	}

	public void setEnabled(boolean enabled) {
		enabledProperty.set(enabled);
	}

	public void setValue(Object value) {
		valueProperty.set(value);
	}

}
