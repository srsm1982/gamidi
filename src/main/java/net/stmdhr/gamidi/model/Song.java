package net.stmdhr.gamidi.model;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.Instrument;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Shruti;
import net.stmdhr.gamidi.ShrutiOctave;
import net.stmdhr.gamidi.swara.Swara;

public class Song {

	private final ObjectProperty<Raaga> raagaProperty = new SimpleObjectProperty<>();
	private final ObjectProperty<Shruti> shrutiProperty = new SimpleObjectProperty<>();
	private final ObjectProperty<ShrutiOctave> shrutiOctaveProperty = new SimpleObjectProperty<>();

	private final IntegerProperty tempoProperty = new SimpleIntegerProperty();
	private final IntegerProperty droneVolumeProperty = new SimpleIntegerProperty();
	private final ObjectProperty<Instrument> instrumentProperty = new SimpleObjectProperty<>();

	private List<Swara> swaras = new ArrayList<>();
	
	private static final Song INSTANCE = new Song();

	private Song() {
	}

	public static Song getInstance() {
		return INSTANCE;
	}

	public List<Swara> getSwaras() {
		return swaras;
	}

	public void setSwaras(List<Swara> swaras) {
		this.swaras = swaras;
	}
	
	public ObjectProperty<Raaga> getRaagaProperty() {
		return raagaProperty;
	}

	public Raaga getRaaga() {
		return raagaProperty.get();
	}

	public void setRaaga(Raaga raaga) {
		raagaProperty.set(raaga);
	}
	
	public ObjectProperty<ShrutiOctave> getShrutiOctaveProperty() {
		return shrutiOctaveProperty;
	}

	public ShrutiOctave getShrutiOctave() {
		return shrutiOctaveProperty.get();
	}

	public void setShrutiOctave(ShrutiOctave shrutiOctave) {
		shrutiOctaveProperty.set(shrutiOctave);
	}
	
	public ObjectProperty<Shruti> getShrutiProperty() {
		return shrutiProperty;
	}

	public Shruti getShruti() {
		return shrutiProperty.get();
	}

	public void setShruti(Shruti shruti) {
		shrutiProperty.set(shruti);
	}
	
	public IntegerProperty getTempoProperty() {
		return tempoProperty;
	}

	public int getTempo() {
		return tempoProperty.get();
	}

	public void setTempo(int tempo) {
		tempoProperty.set(tempo);
	}

	public ObjectProperty<Instrument> getInstrumentProperty() {
		return instrumentProperty;
	}

	public Instrument getInstrument() {
		return instrumentProperty.get();
	}

	public void setInstrument(Instrument instrument) {
		instrumentProperty.set(instrument);
	}
	
	public IntegerProperty getDroneVolumeProperty() {
		return droneVolumeProperty;
	}

	public int getDroneVolume() {
		return droneVolumeProperty.get();
	}

	public void setDroneVolume(int volume) {
		droneVolumeProperty.set(volume);
	}
	
}
