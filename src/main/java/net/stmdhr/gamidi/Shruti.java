package net.stmdhr.gamidi;

public enum Shruti {

	C("C", 0), //
	C1("C#", 1), //
	D("D", 2), //
	D1("D#", 3), //
	E("E", 4), //
	F("F", 5), //
	F1("F#", 6), //
	G("G", 7), //
	G1("G#", 8), //
	A("A", 9), //
	A1("A#", 10), //
	B("B", 11), //
	;
	
	public static final Shruti DEFAULT = F1;

	private final String text;
	private final Integer value;

	private Shruti(String text, Integer value) {
		this.text = text;
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public Integer getValue() {
		return value;
	}

	public static Shruti fromText(String text) {
		Shruti[] values = values();
		for (Shruti shruti : values) {
			if (shruti.text.equals(text)) {
				return shruti;
			}
		}
		return C;
	}

}
