package net.stmdhr.gamidi.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.song.SelectableToggleGroup;
import net.stmdhr.gamidi.swara.Octave;

public class OctaveSelector {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OctaveSelector.class);
	private static final OctaveSelector INSTANCE = new OctaveSelector();

	private final HBox node = new HBox();
	private final SelectableToggleGroup toggleGroup = new SelectableToggleGroup();
	private final ObjectProperty<Octave> valueProperty = new SimpleObjectProperty<>();

	private OctaveSelector() {

		node.setPadding(Style.PADDING_SIDE_5);
		node.setAlignment(Pos.CENTER_LEFT);
		
		Label label = new Label("Octave:");
		node.getChildren().add(label);
		
		HBox toggleBox = new HBox();
		toggleBox.setPadding(Style.PADDING_SIDE_5);
		toggleBox.setAlignment(Pos.CENTER_LEFT);
		node.getChildren().add(toggleBox);

		Octave[] octaves = Octave.values();

		for (int i = 1; i <= octaves.length; i++) {

			Octave octave = octaves[i - 1];
			String value = String.valueOf(octave.getValue());

			ToggleButton toggle = new ToggleButton();
			toggle.setText(value);
			toggle.setUserData(octave);
			toggle.setToggleGroup(toggleGroup);
			toggleBox.getChildren().add(toggle);

			if (octave == Octave.BASE) {
				toggle.setSelected(true);
			}
		}

		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("OctaveSelector.Toggle >> OctaveSelector");
					Octave newOctave = (Octave) newValue.getUserData();
					valueProperty.set(newOctave);
				}
			}
		});
		
		valueProperty.addListener(new ChangeListener<Octave>() {
			@Override
			public void changed(ObservableValue<? extends Octave> observable, Octave oldValue, Octave newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("OctaveSelector >> OctaveSelector.Toggle");
					toggleGroup.setValue(newValue);
				}
			}
		});
	}

	public static OctaveSelector getInstance() {
		return INSTANCE;
	}

	public Node getNode() {
		return node;
	}

	public void addListener(ChangeListener<? super Octave> listener) {
		valueProperty.addListener(listener);
	}

	public Octave getValue() {
		return valueProperty.get();
	}

	public void setValue(Octave octave) {
		valueProperty.set(octave);
	}

	public void setEnabled(boolean enabled) {
		toggleGroup.setEnabled(enabled);
	}

}
