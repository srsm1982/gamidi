package net.stmdhr.gamidi.controls;

import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import net.stmdhr.gamidi.Style;

public class DroneVolumeSelector {

	public static final int DEFAULT_VALUE = 25;
	private static final DroneVolumeSelector INSTANCE = new DroneVolumeSelector();

	private final HBox node = new HBox();
	private final DoubleProperty valueProperty;

	private DroneVolumeSelector() {

		node.setPadding(Style.PADDING_5);

		Slider slider = new Slider(0, 100, 0);
		slider.setShowTickMarks(true);
		slider.setShowTickLabels(true);
		slider.setBlockIncrement(5);
		node.getChildren().add(slider);

		valueProperty = slider.valueProperty();
	}

	public static DroneVolumeSelector getInstance() {
		return INSTANCE;
	}

	public Node getNode() {
		return node;
	}

	public void addListener(ChangeListener<? super Number> listener) {
		valueProperty.addListener(listener);
	}

	public Double getValue() {
		return valueProperty.get();
	}

	public void setValue(Integer value) {
		valueProperty.set(value);
	}
}
