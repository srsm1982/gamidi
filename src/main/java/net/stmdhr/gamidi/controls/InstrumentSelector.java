package net.stmdhr.gamidi.controls;

import javax.sound.midi.Instrument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.song.SelectableToggleGroup;

public class InstrumentSelector {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentSelector.class);
	private static final InstrumentSelector INSTANCE = new InstrumentSelector();

	private final TitledPane node = new TitledPane();
	private final SelectableToggleGroup toggleGroup = new SelectableToggleGroup();
	private final ObjectProperty<Instrument> valueProperty = new SimpleObjectProperty<>();

	private InstrumentSelector() {

		node.setAnimated(false);
		node.setPadding(Style.PADDING_5);
		node.setExpanded(false);

		ScrollPane scroll = new ScrollPane();
		node.setContent(scroll);

		GridPane grid = new GridPane();
		scroll.setContent(grid);

		int perRow = 3;
		int rowIndex = 0;
		int columnIndex = 0;

		Instrument[] instruments = Player.getInstance().getAvailableInstruments();
		for (int i = 1; i <= instruments.length; i++) {

			Instrument instrument = instruments[i - 1];
			String name = instrument.getName().trim();

			ToggleButton toggle = new ToggleButton();
			toggle.setText(name);
			toggle.setUserData(instrument);
			toggle.setToggleGroup(toggleGroup);
			toggle.setAlignment(Pos.CENTER_LEFT);
			toggle.setMaxWidth(Double.MAX_VALUE);
			grid.add(toggle, columnIndex++, rowIndex);

			if (i % perRow == 0) {
				rowIndex++;
				columnIndex = 0;
			}
		}

		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("InstrumentSelector.Toggle >> InstrumentSelector");
					Instrument newInstrument = (Instrument) newValue.getUserData();
					valueProperty.set(newInstrument);
				}
			}
		});

		valueProperty.addListener(new ChangeListener<Instrument>() {
			@Override
			public void changed(ObservableValue<? extends Instrument> observable, Instrument oldValue,
					Instrument newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("InstrumentSelector >> InstrumentSelector.Toggle");
					toggleGroup.setValue(newValue);
				}
			}
		});
		
		valueProperty.addListener(new ChangeListener<Instrument>() {
			@Override
			public void changed(ObservableValue<? extends Instrument> observable, Instrument oldValue,
					Instrument newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("InstrumentSelector >> InstrumentSelector.Title");
					String name = newValue.getName();
					node.setText("Instrument: " + name);
					node.setExpanded(false);
				}
			}
		});
		
	}

	public static InstrumentSelector getInstance() {
		return INSTANCE;
	}

	public Node getNode() {
		return node;
	}

	public void addListener(ChangeListener<? super Instrument> listener) {
		valueProperty.addListener(listener);
	}

	public Instrument getValue() {
		return valueProperty.get();
	}

	public void setValue(Instrument value) {
		valueProperty.set(value);
	}

}
