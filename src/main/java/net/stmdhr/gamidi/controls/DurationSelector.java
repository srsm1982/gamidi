package net.stmdhr.gamidi.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.song.SelectableToggleGroup;
import net.stmdhr.gamidi.swara.Duration;

public class DurationSelector {

	private static final Logger LOGGER = LoggerFactory.getLogger(DurationSelector.class);
	private static final DurationSelector INSTANCE = new DurationSelector();

	private final HBox node  = new HBox();
	private final SelectableToggleGroup toggleGroup = new SelectableToggleGroup();
	private final ObjectProperty<Duration> valueProperty = new SimpleObjectProperty<>();
	
	private DurationSelector() {
		
		node.setPadding(Style.PADDING_SIDE_5);
		node.setAlignment(Pos.CENTER_LEFT);
		
		Label label = new Label("Duration:");
		node.getChildren().add(label);
		
		HBox toggleBox = new HBox();
		toggleBox.setPadding(Style.PADDING_SIDE_5);
		toggleBox.setAlignment(Pos.CENTER_LEFT);
		node.getChildren().add(toggleBox);

		Duration[] durations = Duration.values();
		for (int i = 1; i <= durations.length; i++) {

			Duration duration = durations[i - 1];
			String value = String.valueOf(duration.getSymbol());

			ToggleButton toggle = new ToggleButton();
			toggle.setText(value);
			toggle.setUserData(duration);
			toggle.setToggleGroup(toggleGroup);
			toggleBox.getChildren().add(toggle);

			if (duration == Duration.FULL) {
				toggle.setSelected(true);
			}
		}
		
		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("DurationSelector.Toggle >> DurationSelector");
					Duration newDuration = (Duration) newValue.getUserData();
					valueProperty.set(newDuration);
				}
			}
		});
		
		valueProperty.addListener(new ChangeListener<Duration>() {
			@Override
			public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("DurationSelector >> DurationSelector.Toggle");
					toggleGroup.setValue(newValue);
				}
			}
		});
	}

	public static DurationSelector getInstance() {
		return INSTANCE;
	}
	
	public Node getNode() {
		return node;
	}
	
	public void addListener(ChangeListener<? super Duration> listener) {
		valueProperty.addListener(listener);
	}

	public Duration getValue() {
		return valueProperty.get();
	}

	public void setValue(Duration value) {
		valueProperty.set(value);
	}

	public void setEnabled(boolean enabled) {
		toggleGroup.setEnabled(enabled);
	}
	
}
