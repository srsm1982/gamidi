package net.stmdhr.gamidi.controls;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import net.stmdhr.gamidi.Player;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.data.SongIO;
import net.stmdhr.gamidi.swara.Duration;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.Octave;
import net.stmdhr.gamidi.swara.Swara;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;

public class SwaraTextArea {

	private static final Logger LOGGER = LoggerFactory.getLogger(SwaraTextArea.class);
	private static final DataFormat GAMIDI_FORMAT = new DataFormat("text/gamidi");

	private static final int CELL_DEFAULT_HEIGHT = 32;
	private static final int CELL_DEFAULT_WIDTH = 64;

	private final TitledPane node;
	private final VBox vBox;

	private final Position caret;
	private final Position anchor;
	private final Position active;
	
	// TODO: Remove activeSwara variable & just use the property
	private Swara activeSwara;
	private final ObjectProperty<Swara> activeSwaraProperty = new SimpleObjectProperty<>();

	private Player player = Player.getInstance();
	private Clipboard clipboard = Clipboard.getSystemClipboard();

	private ScrollPane scroll;

	private PlayMode playMode = PlayMode.SINGLE;

	private static final SwaraTextArea INSTANCE = new SwaraTextArea();

	enum PlayMode {
		SINGLE, MULTIPLE
	}

	public static SwaraTextArea getInstance() {
		return INSTANCE;
	}

	private SwaraTextArea() {

		vBox = new VBox();
		vBox.setPadding(Style.PADDING_5);

		scroll = new ScrollPane();
		scroll.setContent(vBox);

		node = new TitledPane();
		node.setText("Swara Editor");
		node.setCollapsible(false);
		node.setPadding(Style.PADDING_5);
		node.setMaxHeight(Double.MAX_VALUE);
		node.setContent(scroll);

		HBox row = createRow();
		vBox.getChildren().add(row);

		anchor = new Position(0, 0);
		caret = new Position(0, 0);
		active = new Position(0, 0);

		updateActiveSwara();
		applyStyles();

		vBox.getStyleClass().add("swara-text-area");
		vBox.setFocusTraversable(true);

		vBox.setOnMouseClicked(gridMouseClickHandler);

		player.addLeadEventListener(new MetaEventListener() {
			@Override
			public void meta(MetaMessage metaMsg) {

				if (PlayMode.SINGLE.equals(playMode)) {
					return;
				}

				if (metaMsg.getType() == Player.CUE_MESSAGE_TYPE) {
					// Cue Event
					if (Arrays.equals(metaMsg.getData(), Swara.SWARA_COMPLETED)) {

						while (true) {

							// Move to right
							Position next = right(caret);

							// Check if we are at the end
							if (next.equals(caret)) {
								return;
							}

							// Update current
							caret.row = next.row;
							caret.col = next.col;

							anchor.row = next.row;
							anchor.col = next.col;

							// Check swara
							Swara swara = getSwara(caret);
							if (!swara.isTrailer()) {
								break;
							}
						}

						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								applyStyles();
							}
						});
					}
				}
			}
		});

	}

	private HBox createRow() {
		HBox hBox = new HBox();
		Swara swara = Swara.createTrailer();
		VBox trailer = createCell(swara);
		hBox.getChildren().add(trailer);
		if (scroll != null) {
			// scroll.setVmax(scroll.getVmax() + 1);
		}
		return hBox;
	}

	private VBox createCell(Swara swara) {
		VBox box = new VBox();
		box.setUserData(swara);
		box.setMinHeight(CELL_DEFAULT_HEIGHT);
		box.setMinWidth(CELL_DEFAULT_WIDTH);
		box.getStyleClass().add("swara-cell");
		box.setOnMouseClicked(cellMouseClickedHandler);
		box.setOnDragDetected(cellDragDetectedHandler);
		box.setOnMouseDragEntered(cellDragEnteredHandler);
		return box;
	}

	EventHandler<MouseEvent> cellDragDetectedHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			vBox.startFullDrag();
		}
	};

	EventHandler<MouseDragEvent> cellDragEnteredHandler = new EventHandler<MouseDragEvent>() {
		@Override
		public void handle(MouseDragEvent event) {
			VBox cell = (VBox) event.getSource();
			Parent row = cell.getParent();
			int rowNum = vBox.getChildrenUnmodifiable().indexOf(row);
			int colNum = row.getChildrenUnmodifiable().indexOf(cell);
			caret.row = rowNum;
			caret.col = colNum;

			applyStyles();
		}
	};

	EventHandler<MouseEvent> cellMouseClickedHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			VBox cell = (VBox) event.getSource();
			HBox row = (HBox) cell.getParent();
			int rowNum = vBox.getChildrenUnmodifiable().indexOf(row);
			int colNum = row.getChildrenUnmodifiable().indexOf(cell);

			boolean isShift = event.isShiftDown();
			caret.row = rowNum;
			caret.col = colNum;
			if (!isShift) {
				anchor.row = caret.row;
				anchor.col = caret.col;
				active.row = caret.row;
				active.col = caret.col;
				updateActiveSwara();
			}

			applyStyles();
		}
	};

	EventHandler<MouseEvent> gridMouseClickHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			vBox.requestFocus();
		}

	};

	public void addSwara(Swara swara) {

		VBox cell = createCell(swara);
		cell.setUserData(swara);
		renderSwara(cell);

		HBox currentRow = (HBox) vBox.getChildren().get(caret.row);
		currentRow.getChildren().add(caret.col, cell);

		caret.col++;

		anchor.row = caret.row;
		anchor.col = caret.col;

		active.col = caret.col - 1;
		active.row = caret.row;
		updateActiveSwara();

		applyStyles();
	}

	public void addActiveSwaraListener(ChangeListener<? super Swara> listener) {
		activeSwaraProperty.addListener(listener);
	}

	private void updateActiveSwara() {

		Swara swara = getSwara(active);
		this.activeSwara = swara;
		activeSwaraProperty.set(activeSwara);

		playActiveSwara();
	}

	private void playActiveSwara() {
		// TODO Remove this method & use interactions
		playMode = PlayMode.SINGLE;
		player.playLead(activeSwara);
	}

	public void backspace() {
		if (caret.equals(anchor)) {

			Position left = left(caret);

			caret.row = left.row;
			caret.col = left.col;

			anchor.row = left.row;
			anchor.col = left.col;

			active.row = left.row;
			active.col = left.col;
			updateActiveSwara();
		}
		deleteSelection();
	}

	public void addNewLine() {

		HBox currentRow = (HBox) vBox.getChildren().get(caret.row);

		HBox newRow = createRow();
		vBox.getChildren().add(caret.row + 1, newRow);

		List<Node> transferCells = new ArrayList<>();
		for (int i = caret.col; i < currentRow.getChildren().size() - 1; i++) {
			Node cell = currentRow.getChildren().get(i);
			transferCells.add(cell);
		}

		currentRow.getChildren().removeAll(transferCells);
		newRow.getChildren().addAll(0, transferCells);

		caret.row++;
		caret.col = 0;

		anchor.row = caret.row;
		anchor.col = caret.col;

		active.row = caret.row;
		active.col = caret.col;
		updateActiveSwara();

		applyStyles();
	}

	private void renderSwara(VBox cell) {

		Object data = cell.getUserData();
		Swara swara = (Swara) data;

		if (!swara.isTrailer()) {

			char note = swara.getNote().getSymbol();
			double duration = swara.getDuration().getValue();
			cell.setMinWidth(CELL_DEFAULT_WIDTH * duration);

			Text text1 = new Text();

			if (swara.getOctave() != null) {
				char octave = swara.getOctave().getSymbol();
				text1.setText(new String(new char[] { note, octave }));
			} else {

				text1.setText(new String(new char[] { note }));
			}
			text1.getStyleClass().add("swara-text");

			Text text2 = new Text();
			if (swara.getGamaka() != null) {
				String gamaka = swara.getGamaka().getSymbol();
				text2.setText(gamaka);
			}
			cell.getChildren().clear();
			cell.getChildren().addAll(text1, text2);

		}
	}

	void clearStyles() {
		ObservableList<Node> rows = vBox.getChildrenUnmodifiable();
		for (int i = 0; i < rows.size(); i++) {
			HBox row = (HBox) rows.get(i);
			ObservableList<Node> cols = row.getChildrenUnmodifiable();
			for (int j = 0; j < cols.size(); j++) {
				VBox cell = (VBox) cols.get(j);
				cell.getStyleClass().clear();
				cell.getStyleClass().add("swara-cell");
			}
		}
	}

	Position left(Position input) {
		int row, col;
		if (input.col > 0) {
			// If there is place to left
			row = input.row;
			col = input.col - 1;
		} else {
			if (input.row == 0) {
				// If this is first row
				row = input.row;
				col = input.col;
			} else {
				// Go up to the last column
				HBox prevRow = (HBox) vBox.getChildren().get(input.row - 1);
				int maxCols = prevRow.getChildren().size();
				row = input.row - 1;
				col = maxCols - 1;
			}
		}
		return new Position(row, col);
	}

	Position right(Position input) {
		int row, col;
		HBox currentRow = (HBox) vBox.getChildren().get(caret.row);
		int maxCols = currentRow.getChildren().size();
		if (input.col < maxCols - 1) {
			// If there is place to right
			row = input.row;
			col = input.col + 1;
		} else {
			int maxRows = vBox.getChildren().size();
			if (input.row == maxRows - 1) {
				// If this is last row
				row = input.row;
				col = input.col;
			} else {
				// Go down to the first column
				row = input.row + 1;
				col = 0;
			}
		}
		return new Position(row, col);
	}

	Position up(Position input) {
		int row, col;
		if (input.row == 0) {
			// If first row
			row = input.row;
			col = input.col;
		} else {
			HBox prevRow = (HBox) vBox.getChildren().get(input.row - 1);
			int maxCols = prevRow.getChildren().size();
			if (input.col < maxCols - 1) {
				// Go straight up
				row = input.row - 1;
				col = input.col;
			} else {
				// Go to end of previous line
				row = input.row - 1;
				col = maxCols - 1;
			}
		}
		return new Position(row, col);
	}

	Position down(Position input) {
		int row, col;
		int maxRows = vBox.getChildren().size();
		if (input.row == maxRows - 1) {
			// If last row
			row = input.row;
			col = input.col;
		} else {
			HBox nextRow = (HBox) vBox.getChildren().get(input.row + 1);
			int maxCols = nextRow.getChildren().size();
			if (input.col < maxCols - 1) {
				// Go straight down
				row = input.row + 1;
				col = input.col;
			} else {
				// Go to end of next line
				row = input.row + 1;
				col = maxCols - 1;
			}
		}
		return new Position(row, col);
	}

	void applyStyles() {

		clearStyles();

		VBox activeCell = getCell(active);
		activeCell.getStyleClass().clear();
		activeCell.getStyleClass().add("swara-cell-active");

		Position min = Position.min(caret, anchor);
		Position max = Position.max(caret, anchor);

		ObservableList<Node> rows = vBox.getChildrenUnmodifiable();
		for (int i = min.row; i <= max.row; i++) {

			HBox row = (HBox) rows.get(i);
			ObservableList<Node> cols = row.getChildrenUnmodifiable();

			for (int j = 0; j < cols.size(); j++) {

				if (i == min.row && j < min.col) {
					// In first row of selection before min
					continue;
				}
				if (i == max.row && j > max.col) {
					// In last row of selection after max
					continue;
				}

				VBox cell = (VBox) cols.get(j);
				cell.getStyleClass().add("swara-cell-selected");
			}
		}

		VBox caretCell = getCell(caret);
		ensureVisibility(caretCell);
	}

	private void ensureVisibility(VBox cell) {

		if (scroll != null) {

			// Parent row = cell.getParent();
			// int rowNum = vBox.getChildrenUnmodifiable().indexOf(row);
			//
			// Bounds scrollViewport = scroll.getViewportBounds();
			// Bounds viewportInScene = scroll.localToScene(scrollViewport);
			//
			// Bounds rowBoundsInParent = row.getBoundsInParent();
			// Bounds rowBoundsInScene = row.localToScene(rowBoundsInParent);
			// Bounds scrollLocalBounds = scroll.getContent().getBoundsInLocal();
			//
			// double scrollViewportHeight = scrollViewport.getHeight();

			// Bounds scrollSceneBounds = scrollContent.localToScene(scrollLocalBounds);
			// double scrollHeight = scrollSceneBounds.getHeight();

			// Bounds cellLocalBounds = cell.getBoundsInLocal();
			// Bounds cellSceneBounds = cell.localToScene(cellLocalBounds);
			// double cellMinY = cellSceneBounds.getMinY();
			// double cellMaxY = cellSceneBounds.getMaxY();
			// LOGGER.debug("scroll={}, cell={}", scrollViewport.getMaxY(),
			// row.getBoundsInLocal().getHeight());

			// double x = cell.getBoundsInParent().getMaxX();
			// scroll.setHvalue(x / scrollLocalWidth);
			//
			// double y = cell.getParent().getBoundsInParent().getMaxY();
			// row.getBoundsInLocal()

		}
	}

	private VBox getCell(Position position) {
		HBox currentRow = (HBox) vBox.getChildren().get(position.row);
		return (VBox) currentRow.getChildren().get(position.col);
	}

	public void deleteSelection() {

		Position min = Position.min(caret, anchor);
		Position max = Position.max(caret, anchor);

		ObservableList<Node> rows = vBox.getChildren();
		List<HBox> deleteRows = new ArrayList<>();
		for (int i = min.row; i <= max.row; i++) {

			HBox row = (HBox) rows.get(i);
			ObservableList<Node> cols = row.getChildren();

			List<VBox> deleteCells = new ArrayList<>();
			for (int j = 0; j < cols.size(); j++) {

				if (i == min.row && j < min.col) {
					// In first row of selection before min
					continue;
				}

				if (i == max.row && j > max.col) {
					// In last row of selection after max
					continue;
				}

				VBox cell = (VBox) cols.get(j);
				deleteCells.add(cell);
			}

			cols.removeAll(deleteCells);

			if (cols.isEmpty()) {
				deleteRows.add(row);
			}

		}

		rows.removeAll(deleteRows);
		scroll.setVmax(scroll.getVmax() - deleteRows.size());

		caret.row = min.row;
		caret.col = min.col;

		if (rows.size() < (caret.row + 1)) {
			// If last row(s) are deleted.
			HBox row = createRow();
			rows.add(caret.row, row);
		}

		HBox currentRow = (HBox) rows.get(caret.row);

		if (currentRow.getChildren().size() < (caret.col + 1)) {
			// If last column(s) are deleted.

			if (rows.size() > caret.row + 1) {
				// If there are rows below.

				// Delete next row.
				HBox nextRow = (HBox) rows.get(caret.row + 1);
				rows.remove(nextRow);

				// Move all cells from next row into this row.
				ObservableList<Node> nextRowCells = nextRow.getChildrenUnmodifiable();
				currentRow.getChildren().addAll(nextRowCells);

			} else {

				// There are no rows below - create a trailer.
				Swara swara = Swara.createTrailer();
				VBox trailer = createCell(swara);
				currentRow.getChildren().add(caret.col, trailer);
			}
		}

		anchor.row = caret.row;
		anchor.col = caret.col;

		active.row = caret.row;
		active.col = caret.col;
		updateActiveSwara();

		applyStyles();
	}

	private Swara getSwara(Position position) {
		VBox cell = getCell(position);
		return (Swara) cell.getUserData();
	}

	private List<Swara> getSelectedSwaras() {
		Position min = Position.min(caret, anchor);
		Position max = Position.max(caret, anchor);
		return getSwaras(min, max);
	}

	private List<Swara> getSwaras(Position min, Position max) {
		List<Swara> swaras = new ArrayList<Swara>();
		ObservableList<Node> rows = vBox.getChildrenUnmodifiable();
		for (int i = min.row; i <= max.row; i++) {
			HBox row = (HBox) rows.get(i);
			ObservableList<Node> cols = row.getChildrenUnmodifiable();
			for (int j = 0; j < cols.size(); j++) {
				if (i == min.row && j < min.col) {
					// In first row of selection before min
					continue;
				}
				if (i == max.row && j > max.col) {
					// In last row of selection after max
					continue;
				}
				VBox cell = (VBox) cols.get(j);
				swaras.add((Swara) cell.getUserData());
			}
		}
		return swaras;
	}

	public List<Swara> getAllSwaras() {
		Position first = new Position(0, 0);
		Position last = getLastPosition();
		return getSwaras(first, last);
	}

	private void deleteAll() {
		ObservableList<Node> rows = vBox.getChildren();
		rows.clear();

		caret.row = 0;
		caret.col = 0;

		anchor.row = 0;
		anchor.col = 0;

		active.row = 0;
		active.col = 0;

		HBox row = createRow();
		rows.add(caret.row, row);

		updateActiveSwara();

		applyStyles();
	}

	public void selectAll() {
		ObservableList<Node> rows = vBox.getChildrenUnmodifiable();
		int row = rows.size() - 1;

		HBox lastRow = (HBox) rows.get(row);
		ObservableList<Node> cols = lastRow.getChildrenUnmodifiable();
		int col = cols.size() - 1;

		anchor.row = 0;
		anchor.col = 0;

		caret.row = row;
		caret.col = col;

		applyStyles();
	}

	public void readAll(List<Swara> swaras) {
		deleteAll();
		paste(swaras);
	}

	private void paste(List<Swara> swaras) {

		for (Swara swara : swaras) {

			if (swara.isTrailer()) {

				HBox currentRow = (HBox) vBox.getChildren().get(caret.row);

				HBox newRow = createRow();
				vBox.getChildren().add(caret.row + 1, newRow);

				List<Node> transferCells = new ArrayList<>();
				for (int i = caret.col; i < currentRow.getChildren().size() - 1; i++) {
					Node cell = currentRow.getChildren().get(i);
					transferCells.add(cell);
				}

				currentRow.getChildren().removeAll(transferCells);
				newRow.getChildren().addAll(0, transferCells);

				caret.row++;
				caret.col = 0;

			} else {

				VBox cell = createCell(swara);
				cell.setUserData(swara);
				renderSwara(cell);

				HBox currentRow = (HBox) vBox.getChildren().get(caret.row);
				currentRow.getChildren().add(caret.col, cell);

				caret.col++;
			}
		}

		active.row = caret.row;
		active.col = caret.col;

		updateActiveSwara();
	}

	public void moveLeft(boolean moveAnchor) {
		Position left = left(caret);

		if (moveAnchor) {
			anchor.row = left.row;
			anchor.col = left.col;
		}

		caret.row = left.row;
		caret.col = left.col;

		active.row = left.row;
		active.col = left.col;
		updateActiveSwara();

		applyStyles();
	}

	public void moveRight(boolean moveAnchor) {
		Position right = right(caret);

		if (moveAnchor) {
			anchor.row = right.row;
			anchor.col = right.col;
		}

		caret.row = right.row;
		caret.col = right.col;

		active.row = right.row;
		active.col = right.col;
		updateActiveSwara();

		applyStyles();
	}

	public void moveDown(boolean moveAnchor) {
		Position down = down(caret);

		if (moveAnchor) {
			anchor.row = down.row;
			anchor.col = down.col;
		}

		caret.row = down.row;
		caret.col = down.col;

		active.row = down.row;
		active.col = down.col;
		updateActiveSwara();

		applyStyles();
	}

	public void moveUp(boolean moveAnchor) {
		Position up = up(caret);

		if (moveAnchor) {
			anchor.row = up.row;
			anchor.col = up.col;
		}

		caret.row = up.row;
		caret.col = up.col;

		active.row = up.row;
		active.col = up.col;
		updateActiveSwara();

		applyStyles();
	}

	public void playFromCaret() {
		List<Swara> swaras = getSwarasFromCaret();
		playMode = PlayMode.MULTIPLE;
		player.playLead(swaras);
	}

	public void stopPlaying() {
		player.stopLead();
	}

	private Position getLastPosition() {
		ObservableList<Node> rows = vBox.getChildrenUnmodifiable();
		HBox lastRow = (HBox) rows.get(rows.size() - 1);
		ObservableList<Node> cols = lastRow.getChildrenUnmodifiable();

		return new Position(rows.size() - 1, cols.size() - 1);
	}

	private List<Swara> getSwarasFromCaret() {
		Position lastPosition = getLastPosition();
		return getSwaras(caret, lastPosition);
	}

	public void copyToClipboard() {

		List<Swara> swaras = getSelectedSwaras();
		String data = SongIO.write(swaras);
		LOGGER.debug("Copied gamidiData={}", data);

		ClipboardContent content = new ClipboardContent();
		content.put(GAMIDI_FORMAT, data);
		clipboard.setContent(content);
	}

	public void pasteFromClipboard() {

		List<Swara> swaras;
		String gamidiContent = (String) clipboard.getContent(GAMIDI_FORMAT);
		String plainContent = (String) clipboard.getContent(DataFormat.PLAIN_TEXT);

		if (StringUtils.isNotEmpty(gamidiContent)) {
			swaras = SongIO.readSwaras(gamidiContent);
		} else {
			swaras = readSimpleSwaras(plainContent);
		}

		paste(swaras);

		anchor.row = caret.row;
		anchor.col = caret.col;

		active.row = caret.row;
		active.col = caret.col;

		updateActiveSwara();
		applyStyles();
	}

	private List<Swara> readSimpleSwaras(String plainContent) {

		List<Swara> swaras = new ArrayList<>();

		if (StringUtils.isNotEmpty(plainContent)) {

			char[] charArray = plainContent.toCharArray();
			Swara swara = null;
			for (char c : charArray) {
				Note note = Note.fromSymbol(c);
				if (note != null) {
					swara = new Swara();
					swara.setNote(note);
					swara.setDuration(Duration.FULL);
					swara.setGamaka(Gamaka.NO_GAMAKA);
					swara.setOctave(Octave.BASE);
					swara.setTrailer(false);
					swaras.add(swara);
				}
			}
		}

		return swaras;
	}

	public void moveToFirst() {

		caret.col = 0;

		active.row = caret.row;
		active.col = caret.col;

		anchor.row = caret.row;
		anchor.col = caret.col;

		updateActiveSwara();
		applyStyles();
	}

	public void moveToLast() {

		HBox currentRow = (HBox) vBox.getChildren().get(caret.row);
		int rowSize = currentRow.getChildren().size();

		// Decrement one for index, one for trailer
		caret.col = rowSize - 2;

		active.row = caret.row;
		active.col = caret.col;

		anchor.row = caret.row;
		anchor.col = caret.col;

		updateActiveSwara();
		applyStyles();

	}

	public Node getNode() {
		return node;
	}

	public void setActiveSwaraDuration(Duration newDuration) {
		Duration currentDuration = activeSwara.getDuration();
		if (!newDuration.equals(currentDuration)) {
			activeSwara.setDuration(newDuration);
			VBox activeCell = getCell(active);
			renderSwara(activeCell);
			playActiveSwara();
		}
	}

	public void setActiveSwaraOctave(Octave newOctave) {
		Octave currentOctave = activeSwara.getOctave();
		if (!newOctave.equals(currentOctave)) {
			activeSwara.setOctave(newOctave);
			VBox activeCell = getCell(active);
			renderSwara(activeCell);
			playActiveSwara();
		}
	}

	public void setActiveSwaraGamaka(Gamaka newGamaka) {
		Gamaka currentGamaka = activeSwara.getGamaka();
		if (!newGamaka.equals(currentGamaka)) {
			activeSwara.setGamaka(newGamaka);
			VBox activeCell = getCell(active);
			renderSwara(activeCell);
			playActiveSwara();
		}
	}

}
