package net.stmdhr.gamidi.controls;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.song.SelectableToggleGroup;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;
import net.stmdhr.gamidi.swara.gamaka.GamakaButton;
import net.stmdhr.gamidi.swara.gamaka.GamakaCategory;
import net.stmdhr.gamidi.swara.gamaka.GamakaScale;

public class GamakaSelector {

	private static final Logger LOGGER = LoggerFactory.getLogger(GamakaSelector.class);
	private static final GamakaSelector INSTANCE = new GamakaSelector();

	public static GamakaSelector getInstance() {
		return INSTANCE;
	}

	private final TitledPane node = new TitledPane();
	private final SelectableToggleGroup toggleGroup = GamakaButton.getGroup();
	private final ObjectProperty<Gamaka> valueProperty = new SimpleObjectProperty<>();

	private GamakaSelector() {

		node.setMaxWidth(Double.MAX_VALUE);
		node.setMaxHeight(Double.MAX_VALUE);
		node.setText("Gamakas");
		node.setExpanded(true);
		node.setCollapsible(false);

		VBox container = new VBox();
		node.setContent(container);

		TabPane tabPane = new TabPane();
		tabPane.setSide(Side.LEFT);
		container.getChildren().add(tabPane);

		GamakaCategory[] categories = GamakaCategory.values();
		for (GamakaCategory category : categories) {

			String name = category.name();
			Tab tab = new Tab(name);
			tab.setClosable(false);
			tabPane.getTabs().add(tab);

			HBox hbox = new HBox(5);
			hbox.setPadding(Style.PADDING_SIDE_10);
			tab.setContent(hbox);

			GamakaScale[] scales = GamakaScale.values();
			for (GamakaScale scale : scales) {

				VBox vbox = new VBox(2);
				vbox.setPrefWidth(50);
				hbox.getChildren().add(vbox);

				Text text = new Text(scale.getText());
				text.setTextAlignment(TextAlignment.RIGHT);
				vbox.getChildren().add(text);

				List<Gamaka> gamakas = Gamaka.values();
				for (Gamaka gamaka : gamakas) {

					GamakaCategory gamakaCategory = gamaka.getCategory();
					GamakaScale gamakaScale = gamaka.getScale();

					if (category.equals(gamakaCategory) && scale.equals(gamakaScale)) {

						String symbol = gamaka.getSymbol();

						ToggleButton toggle = new ToggleButton();
						toggle.setText(symbol);
						toggle.setPrefWidth(35);
						toggle.setUserData(gamaka);
						toggle.setToggleGroup(toggleGroup);
						toggle.setPadding(Style.PADDING_5);
						toggle.setMaxWidth(Double.MAX_VALUE);

						vbox.getChildren().add(toggle);
					}
				}
			}
		}

		HBox shapeBox = new HBox();
		container.getChildren().add(shapeBox);

		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> ov, Toggle oldValue, Toggle newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {

					Gamaka gamaka = (Gamaka) newValue.getUserData();
					int id = gamaka.getId();
					GamakaButton button = GamakaButton.getGamakaButton(id);

					shapeBox.getChildren().clear();
					shapeBox.getChildren().add(button.getShape());
				}
			}
		});
		
		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("GamakaSelector.Toggle >> GamakaSelector");
					Gamaka newGamaka = (Gamaka) newValue.getUserData();
					valueProperty.set(newGamaka);
				}
			}
		});
		
		valueProperty.addListener(new ChangeListener<Gamaka>() {
			@Override
			public void changed(ObservableValue<? extends Gamaka> observable, Gamaka oldValue, Gamaka newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("GamakaSelector >> GamakaSelector.Toggle");
					toggleGroup.setValue(newValue);
				}
			}
		});
	}

	public Node getNode() {
		return node;
	}
	
	public void addListener(ChangeListener<? super Gamaka> listener) {
		valueProperty.addListener(listener);
	}

	public void setEnabled(boolean enabled) {
		toggleGroup.setEnabled(enabled);
	}

	public void setValue(Gamaka value) {
		valueProperty.set(value);
	}

}
