package net.stmdhr.gamidi.controls;

import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import net.stmdhr.gamidi.Shruti;
import net.stmdhr.gamidi.ShrutiOctave;
import net.stmdhr.gamidi.Style;

public class ShrutiSelector {
	
	private static final ShrutiSelector INSTANCE = new ShrutiSelector();

	private final HBox node = new HBox();
	private final ObjectProperty<ShrutiOctave> shrutiOctaveValueProperty;
	private final ObjectProperty<Shruti> shrutiValueProperty;
	
	private ShrutiSelector() {
		
		node.setAlignment(Pos.CENTER_LEFT);
		node.setPadding(Style.PADDING_5);

		{
			ChoiceBox<ShrutiOctave> shrutiOctave = new ChoiceBox<>();
			shrutiOctave.setConverter(new StringConverter<ShrutiOctave>() {
				@Override
				public String toString(ShrutiOctave shrutiOctave) {
					return shrutiOctave.getSymbol();
				}

				@Override
				public ShrutiOctave fromString(String symbol) {
					return ShrutiOctave.fromSymbol(symbol);
				}
			});
			shrutiOctave.getItems().addAll(ShrutiOctave.values());
			shrutiOctaveValueProperty = shrutiOctave.valueProperty();

			node.getChildren().add(shrutiOctave);
		}

		{
			ChoiceBox<Shruti> shruti = new ChoiceBox<>();
			shruti.setConverter(new StringConverter<Shruti>() {
				@Override
				public String toString(Shruti value) {
					return value.getText();
				}

				@Override
				public Shruti fromString(String value) {
					return Shruti.fromText(value);
				}
			});
			shruti.getItems().addAll(Shruti.values());
			shrutiValueProperty = shruti.valueProperty();

			node.getChildren().add(shruti);
		}
	}

	public static ShrutiSelector getInstance() {
		return INSTANCE;
	}
	
	public Node getNode() {
		return node;
	}

	public void addShrutiListener(ChangeListener<? super Shruti> listener) {
		shrutiValueProperty.addListener(listener);
	}

	public Shruti getShrutiValue() {
		return shrutiValueProperty.get();
	}

	public void setShrutiValue(Shruti value) {
		shrutiValueProperty.set(value);
	}
	
	public void addShrutiOctaveListener(ChangeListener<? super ShrutiOctave> listener) {
		shrutiOctaveValueProperty.addListener(listener);
	}

	public ShrutiOctave getShrutiOctaveValue() {
		return shrutiOctaveValueProperty.get();
	}

	public void setShrutiOctaveValue(ShrutiOctave value) {
		shrutiOctaveValueProperty.set(value);
	}

}
