package net.stmdhr.gamidi.controls;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Style;
import net.stmdhr.gamidi.song.SelectableToggleGroup;

public class RaagaSelector {

	private static final Logger LOGGER = LoggerFactory.getLogger(RaagaSelector.class);
	private static final RaagaSelector INSTANCE = new RaagaSelector();

	private final TitledPane node;
	private final SelectableToggleGroup toggleGroup;
	private final ObjectProperty<Raaga> valueProperty = new SimpleObjectProperty<>();

	private RaagaSelector() {

		node = new TitledPane();
		node.setAnimated(false);
		node.setPadding(Style.PADDING_5);
		node.setExpanded(false);

		ScrollPane scroll = new ScrollPane();
		node.setContent(scroll);

		GridPane grid = new GridPane();
		scroll.setContent(grid);

		toggleGroup = new SelectableToggleGroup();

		int perRow = 2;
		int rowIndex = 0;
		int columnIndex = 0;

		List<Raaga> raagas = Raaga.values();
		for (int i = 1; i <= raagas.size(); i++) {

			Raaga raaga = raagas.get(i - 1);
			String name = raaga.getName();
			String description = raaga.getDescription();
			String text = name + "\n" + description;

			ToggleButton toggle = new ToggleButton();
			toggle.setText(text);
			toggle.setUserData(raaga);
			toggle.setToggleGroup(toggleGroup);
			toggle.setMaxWidth(Double.MAX_VALUE);
			toggle.setAlignment(Pos.CENTER_LEFT);
			grid.add(toggle, columnIndex++, rowIndex);

			if (i % perRow == 0) {
				rowIndex++;
				columnIndex = 0;
			}
		}
		
		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("RaagaSelector.Toggle >> RaagaSelector");
					Raaga newRaaga = (Raaga) newValue.getUserData();
					valueProperty.set(newRaaga);
				}
			}
		});

		valueProperty.addListener(new ChangeListener<Raaga>() {
			@Override
			public void changed(ObservableValue<? extends Raaga> observable, Raaga oldValue, Raaga newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("RaagaSelector >> RaagaSelector.Toggle");
					toggleGroup.setValue(newValue);
				}
			}
		});
		
		valueProperty.addListener(new ChangeListener<Raaga>() {
			@Override
			public void changed(ObservableValue<? extends Raaga> observable, Raaga oldValue,
					Raaga newValue) {
				if (newValue != null && !newValue.equals(oldValue)) {
					LOGGER.trace("RaagaSelector >> RaagaSelector.Title");
					String name = newValue.getName();
					node.setText("Raaga: " + name);
					node.setExpanded(false);
				}
			}
		});
	}

	public static RaagaSelector getInstance() {
		return INSTANCE;
	}

	public Node getNode() {
		return node;
	}

	public void addListener(ChangeListener<? super Raaga> listener) {
		valueProperty.addListener(listener);
	}

	public Raaga getValue() {
		return valueProperty.get();
	}

	public void setValue(Raaga value) {
		valueProperty.set(value);
	}
}
