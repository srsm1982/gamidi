package net.stmdhr.gamidi.controls;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import net.stmdhr.gamidi.Style;

public class TempoSelector {

	public static final Integer DEFAULT_VALUE = 140;
	private static final TempoSelector INSTANCE = new TempoSelector();

	private final HBox node = new HBox();
	private final ObjectProperty<Integer> valueProperty;

	private TempoSelector() {

		node.setPadding(Style.PADDING_5);

		ComboBox<Integer> tempo = new ComboBox<>();
		tempo.setPadding(Style.PADDING_5);
		node.getChildren().add(tempo);
		
		List<Integer> range = IntStream.rangeClosed(50, 200).boxed().collect(Collectors.toList());
		tempo.getItems().addAll(range);

		valueProperty = tempo.valueProperty();
	}

	public static TempoSelector getInstance() {
		return INSTANCE;
	}

	public Node getNode() {
		return node;
	}

	public void addListener(ChangeListener<? super Integer> listener) {
		valueProperty.addListener(listener);
	}

	public Integer getValue() {
		return valueProperty.get();
	}

	public void setValue(Integer value) {
		valueProperty.set(value);
	}
}
