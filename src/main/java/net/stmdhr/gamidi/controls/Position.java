package net.stmdhr.gamidi.controls;

public class Position {
	
	int row;
	int col;

	public Position(int row, int col) {
		this.row = row;
		this.col = col;
	}

	@Override
	public String toString() {
		return "Position [row=" + row + ", col=" + col + "]";
	}

	public boolean equals(Position other) {
		return col == other.col && row == other.row;
	}
	
	static Position min(Position a, Position b) {
		if (a.row < b.row) {
			return a;
		}
		if (b.row < a.row) {
			return b;
		}
		if (a.col < b.col) {
			return a;
		}
		if (b.col < a.col) {
			return b;
		}
		return a;
	}

	static Position max(Position a, Position b) {
		if (a.row < b.row) {
			return b;
		}
		if (b.row < a.row) {
			return a;
		}
		if (a.col < b.col) {
			return b;
		}
		if (b.col < a.col) {
			return a;
		}
		return b;
	}

}