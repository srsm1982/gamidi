package net.stmdhr.gamidi;

public enum ShrutiOctave {

	S0("0", 1), //
	S1("1", 2), //
	S2("2", 3), //
	S3("3", 4), //
	S4("4", 5), //
	S5("5", 6), //
	S6("6", 7), //
	S7("7", 8), //
	S8("8", 9) //
	;
	
	public static final ShrutiOctave DEFAULT = S4;

	private final String symbol;
	private final Integer value;

	ShrutiOctave(String symbol, Integer value) {
		this.symbol = symbol;
		this.value = value;
	}

	public String getSymbol() {
		return symbol;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public String toString() {
		return this.name();
	}

	public static ShrutiOctave fromSymbol(String symbol) {
		ShrutiOctave[] values = values();
		for (ShrutiOctave shrutiOctave : values) {
			if (shrutiOctave.symbol == symbol) {
				return shrutiOctave;
			}
		}
		return S4;
	}
}