package net.stmdhr.gamidi;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stmdhr.gamidi.data.RaagaIO;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;

public class Raaga {

	private static final ArrayList<Raaga> VALUES = new ArrayList<>();
	private static final Logger LOGGER = LoggerFactory.getLogger(Gamaka.class);
	public static final Raaga DEFAULT;

	private final String id;
	private final String name;
	private final String description;
	private final int[] semitones;

	/**
	 * Array of Pitch Bend Functions for each swara in this raaga. Each Pitch Bend
	 * Function the Pitch Bend for a given Note Bend.
	 * 
	 * <pre>
	 * For e.g., in Dheerasankarabharanam: 
	 * For S, Note Bend of 1 = Pitch Bend of 2 semi-tones 
	 * For G, Note Bend of 1 = Pitch Bend of 1 semi-tones
	 * </pre>
	 */
	private final UnivariateFunction[] pitchBendFunctions;

	private final Map<Integer, List<Integer>> suggestedGamakasMap;

	static {
		loadFromFileSystem();
		DEFAULT = valueOf("28");
	}

	public Raaga(String id, String name, String description, int[] semitones,
			Map<Integer, List<Integer>> suggestedGamakasMap) {

		this.id = id;
		this.name = name;
		this.description = description;
		this.semitones = semitones;

		if (suggestedGamakasMap == null) {
			suggestedGamakasMap = new HashMap<>();
		}

		Note[] notes = Note.values();
		for (Note note : notes) {
			Integer noteValue = note.getValue();
			List<Integer> list = suggestedGamakasMap.get(noteValue);

			if (list == null) {
				list = new ArrayList<Integer>();
				suggestedGamakasMap.put(noteValue, list);
			}

			if (!list.contains(Gamaka.NO_GAMAK_ID)) {
				list.add(0, Gamaka.NO_GAMAK_ID);
			}
		}

		this.suggestedGamakasMap = suggestedGamakasMap;

		this.pitchBendFunctions = new UnivariateFunction[7];
		
		for (int i = 0; i < 7; i++) {

			int fromSemitone = semitones[i];

			double x[] = new double[15];
			double y[] = new double[15];

			int count = 0;
			for (int j = -7; j <= 7; j++) {

				int index = Math.floorMod(i + j, 7);
				int cycles = Math.floorDiv(i + j, 7);
				int toSemitone = cycles * 12 + semitones[index];

				int semitoneBend = toSemitone - fromSemitone;
				int pitchBend = (int) (8192 + (semitoneBend * 8191) / 12.0);

				x[count] = j;
				y[count] = pitchBend;

				count++;
			}

			LinearInterpolator interpolator = new LinearInterpolator();
			pitchBendFunctions[i] = interpolator.interpolate(x, y);
		}
	}

	private static void loadFromFileSystem() {

		VALUES.clear();

		String raagaDir = Settings.getRaagaDirectory();
		LOGGER.debug("Loading raagas from raagaDir={}", raagaDir);

		File folder = new File(raagaDir);
		File[] files = folder.listFiles();
		for (File file : files) {
			String fileName = file.getName();
			if (file.isFile() && fileName.endsWith(".rg.json")) {
				try {
					String content = FileUtils.readFileToString(file, Charset.forName("UTF-8"));
					Raaga raaga = RaagaIO.readRaaga(content);
					VALUES.add(raaga);
				} catch (Exception e) {
					LOGGER.error("Error parsing fileName={}", fileName, e);
				}
			}
		}
	}

	public static List<Raaga> values() {
		return new ArrayList<Raaga>(VALUES);
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int[] getSemitones() {
		return semitones;
	}

	/**
	 * Converts a Note Bend into Pitch Bend for this raaga.
	 * 
	 * @param note
	 * @param noteBend
	 * @return
	 */
	public int getPitchBend(Note note, double noteBend) {
		UnivariateFunction function = pitchBendFunctions[note.getValue()];
		return (int) function.value(noteBend);
	}
	
	public String getId() {
		return id;
	}

	public static Raaga valueOf(String id) {
		for (Raaga raaga : VALUES) {
			if (raaga.id.equals(id)) {
				return raaga;
			}
		}
		return null;
	}

	public Map<Integer, List<Integer>> getSuggestedGamakasMap() {
		return suggestedGamakasMap;
	}

	public List<Integer> getSuggestedGamakas(Note note) {
		return suggestedGamakasMap.get(note.getValue());
	}

}
