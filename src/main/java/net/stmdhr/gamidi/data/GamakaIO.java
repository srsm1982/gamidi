package net.stmdhr.gamidi.data;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.type.TypeReference;

import net.stmdhr.gamidi.swara.gamaka.Gamaka;
import net.stmdhr.gamidi.swara.gamaka.GamakaCategory;
import net.stmdhr.gamidi.swara.gamaka.GamakaScale;
import net.stmdhr.gamidi.swara.gamaka.PitchPoint;

public class GamakaIO {

	private static ObjectMapper MAPPER = new ObjectMapper();

	static {
		SimpleModule module = new SimpleModule("GamakaIO", new Version(1, 1, 1, ""));
		module.addSerializer(Gamaka.class, new GamakaSerializer());
		module.addDeserializer(Gamaka.class, new GamakaDeserializer());
		MAPPER.registerModule(module);
		MAPPER.enable(Feature.INDENT_OUTPUT);
	}

	private static class GamakaSerializer extends JsonSerializer<Gamaka> {

		@Override
		public void serialize(Gamaka gamaka, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {

			jgen.writeStartObject();
			jgen.writeStringField("v", "1.0");

			int id = gamaka.getId();
			jgen.writeNumberField("id", id);

			String name = gamaka.getName();
			jgen.writeStringField("name", name);

			String symbol = gamaka.getSymbol();
			jgen.writeStringField("symbol", symbol);

			String description = gamaka.getDescription();
			jgen.writeStringField("description", description);

			GamakaCategory category = gamaka.getCategory();
			jgen.writeObjectField("category", category);
			
			GamakaScale scale = gamaka.getScale();
			jgen.writeObjectField("scale", scale);

			List<PitchPoint> pitchPoints = gamaka.getPitchPoints();
			jgen.writeObjectField("pitchPoints", pitchPoints);
			
			jgen.writeEndObject();
		}
	}

	private static class GamakaDeserializer extends JsonDeserializer<Gamaka> {

		@Override
		public Gamaka deserialize(JsonParser jp, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {

			JsonNode node = jp.getCodec().readTree(jp);

			JsonNode idNode = node.get("id");
			int id = idNode.asInt();

			JsonNode nameNode = node.get("name");
			String name = nameNode.asText();

			JsonNode symbolNode = node.get("symbol");
			String symbol = symbolNode.asText();
			
			JsonNode descNode = node.get("description");
			String desc = "";
			if (descNode != null) {
				desc = descNode.asText();
			}

			JsonNode categoryNode = node.get("category");
			GamakaCategory category = GamakaCategory.NONE;
			if (categoryNode != null) {
				String categoryText = categoryNode.asText();
				category = GamakaCategory.valueOf(categoryText);
			}
			
			JsonNode scaleNode = node.get("scale");
			GamakaScale scale = GamakaScale.BASE;
			if (scaleNode != null) {
				String scaleText = scaleNode.asText();
				scale = GamakaScale.valueOf(scaleText);
			}
			
			JsonNode pitchPointsNode = node.get("pitchPoints");
			List<PitchPoint> pitchPoints = MAPPER.readValue(pitchPointsNode, new TypeReference<List<PitchPoint>>() {
			});

			return new Gamaka(id, name, symbol, desc, category, scale, pitchPoints);
		}
	}

	public static String write(Gamaka gamaka) throws Exception {
		return MAPPER.writeValueAsString(gamaka);
	}

	public static Gamaka readGamaka(String content) throws Exception {
		return MAPPER.readValue(content, new TypeReference<Gamaka>() {
		});
	}
}
