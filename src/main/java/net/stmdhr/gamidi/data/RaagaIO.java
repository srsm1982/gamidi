package net.stmdhr.gamidi.data;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stmdhr.gamidi.Raaga;

public class RaagaIO {

	private static final Logger LOGGER = LoggerFactory.getLogger(RaagaIO.class);
	private static ObjectMapper MAPPER = new ObjectMapper();

	static {
		SimpleModule module = new SimpleModule("custom", new Version(1, 1, 1, ""));
		module.addSerializer(Raaga.class, new RaagaSerializer());
		module.addDeserializer(Raaga.class, new RaagaDeserializer());
		MAPPER.registerModule(module);
		MAPPER.enable(Feature.INDENT_OUTPUT);
	}

	private static class RaagaSerializer extends JsonSerializer<Raaga> {

		@Override
		public void serialize(Raaga raaga, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {

			jgen.writeStartObject();

			jgen.writeStringField("v", "1.0");
			jgen.writeStringField("id", raaga.getId());
			jgen.writeStringField("name", raaga.getName());
			jgen.writeStringField("description", raaga.getDescription());
			jgen.writeObjectField("semitones", raaga.getSemitones());
			jgen.writeObjectField("suggestedGamakasMap", raaga.getSuggestedGamakasMap());

			jgen.writeEndObject();
		}
	}

	private static class RaagaDeserializer extends JsonDeserializer<Raaga> {

		@Override
		public Raaga deserialize(JsonParser jp, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {

			JsonNode node = jp.getCodec().readTree(jp);

			String id = node.get("id").asText();
			String name = node.get("name").asText();
			String description = node.get("description").asText();
			int[] semitones = MAPPER.readValue(node.get("semitones"), new TypeReference<int[]>() {
			});

			Map<Integer, List<Integer>> suggestedGamakasMap;
			JsonNode suggestedGamakasMapNode = node.get("suggestedGamakasMap");
			
			if (suggestedGamakasMapNode != null) {
				suggestedGamakasMap = MAPPER.readValue(suggestedGamakasMapNode,
						new TypeReference<Map<Integer, List<Integer>>>() {
						});
			} else {
				suggestedGamakasMap = null;
			}

			return new Raaga(id, name, description, semitones, suggestedGamakasMap);
		}
	}

	public static String write(Raaga raaga) {
		try {
			return MAPPER.writeValueAsString(raaga);
		} catch (Exception e) {
			LOGGER.error("Error writing raaga={}", raaga, e);
			return "";
		}
	}

	public static Raaga readRaaga(String content) {
		try {
			return MAPPER.readValue(content, new TypeReference<Raaga>() {
			});
		} catch (Exception e) {
			LOGGER.error("Error reading content={}", content, e);
			return null;
		}
	}

}
