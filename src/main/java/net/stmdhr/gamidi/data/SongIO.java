package net.stmdhr.gamidi.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.stmdhr.gamidi.Raaga;
import net.stmdhr.gamidi.Shruti;
import net.stmdhr.gamidi.ShrutiOctave;
import net.stmdhr.gamidi.controls.TempoSelector;
import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.Duration;
import net.stmdhr.gamidi.swara.Note;
import net.stmdhr.gamidi.swara.Octave;
import net.stmdhr.gamidi.swara.Swara;
import net.stmdhr.gamidi.swara.gamaka.Gamaka;

public class SongIO {

	private static final Logger LOGGER = LoggerFactory.getLogger(SongIO.class);
	private static ObjectMapper MAPPER = new ObjectMapper();

	static {
		SimpleModule module = new SimpleModule("custom", new Version(1, 1, 1, ""));
		module.addSerializer(Song.class, new SongSerializer());
		module.addSerializer(Swara.class, new SwaraSerializer());
		module.addDeserializer(Swara.class, new SwaraDeserializer());
		MAPPER.registerModule(module);
		MAPPER.enable(Feature.INDENT_OUTPUT);
	}

	private static class SongSerializer extends JsonSerializer<Song> {

		@Override
		public void serialize(Song song, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {

			jgen.writeStartObject();

			jgen.writeStringField("v", "1.0");
			jgen.writeStringField("raaga", song.getRaaga().getId());
			jgen.writeStringField("shrutiOctave", song.getShrutiOctave().name());
			jgen.writeStringField("shruti", song.getShruti().name());
			jgen.writeNumberField("tempo", song.getTempo());
			jgen.writeObjectField("swaras", song.getSwaras());

			jgen.writeEndObject();
		}
	}

	private static class SwaraSerializer extends JsonSerializer<Swara> {

		@Override
		public void serialize(Swara swara, JsonGenerator jgen, SerializerProvider provider)
				throws IOException, JsonProcessingException {

			jgen.writeStartObject();

			boolean isTrailer = swara.isTrailer();
			jgen.writeBooleanField("isTrailer", isTrailer);

			if (!isTrailer) {

				Note note = swara.getNote();
				jgen.writeStringField("note", note.name());

				Duration duration = swara.getDuration();
				jgen.writeStringField("duration", duration.name());

				if (!note.equals(Note.REST)) {

					Octave octave = swara.getOctave();
					jgen.writeStringField("octave", octave.name());

					Gamaka gamaka = swara.getGamaka();
					jgen.writeNumberField("gamaka", gamaka.getId());
				}
			}

			jgen.writeEndObject();
		}
	}

	private static class SwaraDeserializer extends JsonDeserializer<Swara> {

		@Override
		public Swara deserialize(JsonParser jp, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {

			Swara swara = new Swara();
			JsonNode node = jp.getCodec().readTree(jp);

			boolean isTrailer = node.get("isTrailer").asBoolean();
			swara.setTrailer(isTrailer);

			if (!isTrailer) {

				Note note = Note.valueOf(node.get("note").asText());
				Duration duration = Duration.valueOf(node.get("duration").asText());

				if (!note.equals(Note.REST)) {

					Octave octave = Octave.valueOf(node.get("octave").asText());
					swara.setOctave(octave);

					Gamaka gamaka = Gamaka.valueOf(node.get("gamaka").asInt());
					swara.setGamaka(gamaka);
				}

				swara.setNote(note);
				swara.setDuration(duration);

			}

			return swara;
		}
	}

	public static String write(Song song) {
		try {
			// TODO: Change this method to use low level json nodes.
			return MAPPER.writeValueAsString(song);
		} catch (Exception e) {
			LOGGER.error("Error writing song={}", song, e);
			return "";
		}
	}

	public static void readSong(String content) throws IOException {

		try {
			
			JsonNode root = MAPPER.readTree(content);

			String raagaNode = getNodeValue(root, "raaga");
			String shrutiNode = getNodeValue(root, "shruti");
			String shrutiOctaveNode = getNodeValue(root, "shrutiOctave");
			String tempoNode = getNodeValue(root, "tempo");
			JsonNode swarasNode = root.get("swaras");

			Raaga raaga = Raaga.valueOf(raagaNode);
			Shruti shruti = Shruti.valueOf(shrutiNode);
			ShrutiOctave shrutiOctave = ShrutiOctave.valueOf(shrutiOctaveNode);
			Integer tempo = NumberUtils.toInt(tempoNode, TempoSelector.DEFAULT_VALUE);
			List<Swara> swaras = MAPPER.readValue(swarasNode, new TypeReference<List<Swara>>() {
			});

			Song song = Song.getInstance();
			song.setRaaga(raaga);
			song.setShruti(shruti);
			song.setShrutiOctave(shrutiOctave);
			song.setTempo(tempo);
			song.setSwaras(swaras);
			
		} catch (Exception e) {

			throw new IOException(e);
		}
	}

	private static String getNodeValue(JsonNode parent, String nodeName) {
		final JsonNode node = parent.get(nodeName);
		return node != null ? node.asText() : "";
	}

	public static String write(List<Swara> swaras) {
		try {
			
			return MAPPER.writeValueAsString(swaras);
		} catch (Exception e) {
			LOGGER.error("Error writing swaras={}", swaras, e);
			return "";
		}
	}

	public static List<Swara> readSwaras(String content) {
		try {
			return MAPPER.readValue(content, new TypeReference<List<Swara>>() {
			});
		} catch (Exception e) {
			LOGGER.error("Error reading content={}", content, e);
			return new ArrayList<Swara>();
		}
	}
}
