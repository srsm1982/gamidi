package net.stmdhr.gamidi;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.stage.FileChooser.ExtensionFilter;

public class Settings {
	
	public static final ExtensionFilter GAMAKA_FILTER = new ExtensionFilter("Gamaka Files", "*.gmk.json");
	public static final ExtensionFilter SONG_FILTER = new ExtensionFilter("Song Files", "*.sng.json");
	public static final ExtensionFilter RAAGA_FILTER = new ExtensionFilter("Raaga Files", "*.rg.json");

	private static final Logger LOGGER = LoggerFactory.getLogger(Settings.class);
	private static final Properties PROPS = new Properties();
	
	static {
		try {
			String currentDir = System.getProperty("user.dir");
			File file = new File(currentDir, "application.properties");
			PROPS.load(new FileReader(file));
		} catch (Exception e) {
			LOGGER.error("Error initializing settings", e);
			System.out.println("Error initializing settings.");
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static String getSongsDirectory() {
//		return System.getProperty("user.home");
		return "songs";
	}
	
	public static String getGamakaDirectory() {
		return "gamakas";
	}

	public static String getRaagaDirectory() {
		return "raagas";
	}

	public static String getDroneSetting() {
		return PROPS.getProperty("drone.setting");
	}

	public static int getDroneTempo() {
		return Integer.valueOf(PROPS.getProperty("drone.tempo"));
	}

	public static String getTamburaSoundBank() {
		return "instruments/tamburas.sf2";
	}
	
	public static String getVeenasSoundBank() {
		return "instruments/veenas.sf2";
	}
	
}
