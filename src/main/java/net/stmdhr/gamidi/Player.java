package net.stmdhr.gamidi;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.midi.Instrument;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Patch;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;
import javax.sound.midi.Transmitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.media.sound.SF2Instrument;
import com.sun.media.sound.SF2Soundbank;

import net.stmdhr.gamidi.model.Song;
import net.stmdhr.gamidi.swara.Swara;

@SuppressWarnings("restriction")
public class Player {

	private static final Logger LOGGER = LoggerFactory.getLogger(Player.class);

	private static final int LEAD_CHANNEL = 0;
	private static final int DRONE_CHANNEL = 1;

	public static final int PLAYER_RESOLUTION = 128;

	public static final int CUE_MESSAGE_TYPE = 0x07;
	public static final int END_OF_TRACK_MESSAGE_TYPE = 0x2F;

	private int tempo;
	private final Synthesizer synthesizer;
	private final Sequencer leadSequencer;
	private final Sequencer droneSequencer;

	private static final Player INSTANCE = new Player();

	public static Player getInstance() {
		return INSTANCE;
	}

	private Player() {

		try {

			LOGGER.debug("Creating synthesizer, sequencer");
			synthesizer = MidiSystem.getSynthesizer();
			leadSequencer = MidiSystem.getSequencer(false);
			droneSequencer = MidiSystem.getSequencer(false);

			LOGGER.debug("Hookup lead synthesizer to sequencer");
			Transmitter leadTransmitter = leadSequencer.getTransmitter();
			Receiver synthReceiver1 = synthesizer.getReceiver();
			leadTransmitter.setReceiver(synthReceiver1);

			LOGGER.debug("Hookup lead synthesizer to sequencer");
			Transmitter droneTransmitter = droneSequencer.getTransmitter();
			Receiver synthReceiver2 = synthesizer.getReceiver();
			droneTransmitter.setReceiver(synthReceiver2);

			LOGGER.debug("Opening synthesizer, sequencer");
			synthesizer.open();
			leadSequencer.open();
			droneSequencer.open();

			{
				File tamburaFile = new File(Settings.getTamburaSoundBank());
				SF2Soundbank tambura = new SF2Soundbank(tamburaFile);

				Patch patch = new Patch(128, 0);
				tambura.getInstruments()[0].setPatch(patch);

				synthesizer.loadInstruments(tambura, new Patch[] { patch });

				MidiChannel channel = synthesizer.getChannels()[DRONE_CHANNEL];
				channel.programChange(128, 0);
			}

			{
				File file = new File(Settings.getVeenasSoundBank());
				SF2Soundbank bank = new SF2Soundbank(file);

				List<Patch> patches = new ArrayList<>();
				SF2Instrument[] instruments = bank.getInstruments();
				int sizeOfInstruments = instruments.length;
				int program = 0;
				for (SF2Instrument instrument : instruments) {
					Patch patch = new Patch(256, program);
					instrument.setPatch(patch);
					patches.add(patch);
					program++;
				}

				Patch[] patchesArray = patches.toArray(new Patch[sizeOfInstruments]);
				synthesizer.loadInstruments(bank, patchesArray);
			}

			{
				final Instrument[] instruments = synthesizer.getLoadedInstruments();
				LOGGER.debug("Loaded instruments={}", Arrays.toString(instruments));
			}

			{
				LOGGER.debug("Set to mono");
				ShortMessage message = new ShortMessage();
				message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 126, 127);
				synthReceiver1.send(message, -1);
			}

			{
				LOGGER.debug("Set pitch-sensitivity to 12 semitones");
				{
					ShortMessage message = new ShortMessage();
					message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 101, 0);
					synthReceiver1.send(message, -1);
				}
				{
					ShortMessage message = new ShortMessage();
					message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 100, 0);
					synthReceiver1.send(message, -1);
				}
				{
					ShortMessage message = new ShortMessage();
					message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 6, 12);
					synthReceiver1.send(message, -1);
				}
				{
					ShortMessage message = new ShortMessage();
					message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 38, 0);
					synthReceiver1.send(message, -1);
				}
			}

		} catch (Exception e) {
			throw new RuntimeException("Error initializing player", e);
		}
	}

	public Instrument getDefaultInstrument() {
		Instrument defaultInstrument = null;
		final Instrument[] instruments = getAvailableInstruments();
		for (Instrument instrument : instruments) {
			defaultInstrument = instrument;
			if ("Veena".equalsIgnoreCase(instrument.getName())) {
				break;
			}
		}
		return defaultInstrument;
	}
	
	public void setDroneVolume(int volume) {
		int volumeVal = (int) ((volume / 100.0) * 127);
		MidiChannel channel = synthesizer.getChannels()[DRONE_CHANNEL];
		channel.controlChange(7, volumeVal);
	}

	public void setInstrument(Instrument instrument) {
		Patch patch = instrument.getPatch();
		int bank = patch.getBank();
		int program = patch.getProgram();

		LOGGER.debug("Set instrument={}", instrument);
		MidiChannel channel = synthesizer.getChannels()[LEAD_CHANNEL];
		channel.programChange(bank, program);
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public void addLeadEventListener(MetaEventListener listener) {
		leadSequencer.addMetaEventListener(listener);
	}

	public Instrument[] getAvailableInstruments() {
		return synthesizer.getLoadedInstruments();
	}

	public void playLead(List<Swara> swaras) {

		try {
			leadSequencer.stop();

			Sequence sequence = new Sequence(Sequence.PPQ, PLAYER_RESOLUTION);
			Track track = sequence.createTrack();
			long startTick = 0L;

			for (Swara swara : swaras) {
				if (!swara.isTrailer()) {
					long endTick = swara.addMidiEvents(startTick, track);
					startTick = endTick;
				}
			}

			LOGGER.debug("Playing swaras={}", swaras);
			leadSequencer.setSequence(sequence);
			leadSequencer.setTempoInBPM(tempo);
			leadSequencer.start();

		} catch (InvalidMidiDataException e) {
			LOGGER.error("Error playing swaras.", e);
		}
	}

	public void playLead(Swara swara) {

		if (swara.isTrailer()) {
			return;
		}

		try {
			leadSequencer.stop();

			Sequence sequence = new Sequence(Sequence.PPQ, PLAYER_RESOLUTION);
			Track track = sequence.createTrack();
			swara.addMidiEvents(LEAD_CHANNEL, track);

			LOGGER.debug("Playing swara={}", swara);
			leadSequencer.setSequence(sequence);
			leadSequencer.setTempoInBPM(tempo);
			leadSequencer.start();

		} catch (Exception e) {
			LOGGER.error("Error playing swara={}", swara, e);
		}
	}

	public List<MidiEvent> getInstrumentSetupEvents(Instrument instrument) {

		// TODO This is not working.
		ArrayList<MidiEvent> events = new ArrayList<>();

		try {

			// Set instrument
			Patch patch = instrument.getPatch();
			int bank = patch.getBank();
			int program = patch.getProgram();

			{
				// Bank MSB
				ShortMessage message = new ShortMessage();
				message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 0, bank >> 7);
				MidiEvent event = new MidiEvent(message, 0);
				events.add(event);
			}

			{
				// Bank LSB
				ShortMessage message = new ShortMessage();
				message.setMessage(ShortMessage.CONTROL_CHANGE, LEAD_CHANNEL, 32, bank & 0b1111111);
				MidiEvent event = new MidiEvent(message, 0);
				events.add(event);
			}

			{
				// Program
				ShortMessage message = new ShortMessage();
				message.setMessage(ShortMessage.PROGRAM_CHANGE, LEAD_CHANNEL, program >> 7, program & 0b1111111);
				MidiEvent event = new MidiEvent(message, 0);
				events.add(event);
			}

		} catch (InvalidMidiDataException e) {
			LOGGER.error("Invalid midi", e);
		}

		return events;
	}

	public void startDrone() {
		try {
			

			Sequence sequence = new Sequence(Sequence.PPQ, 1);
			Track track = sequence.createTrack();

			String droneSetting = Settings.getDroneSetting();
			int droneTempo = Settings.getDroneTempo();

			// TODO Shruti & ShrutiOctave must be obtained via a setter
			final Song song = Song.getInstance();
			int shrutiOctave = song.getShrutiOctave().getValue();
			int shruti = song.getShruti().getValue();

			String[] events = droneSetting.split(",");
			for (String eventStr : events) {

				String[] eventStrArr = eventStr.split(":");
				int keyOffset = Integer.valueOf(eventStrArr[0]);
				int tick = Integer.valueOf(eventStrArr[1]);
				boolean on = "on".equalsIgnoreCase(eventStrArr[2]);

				int command = on ? ShortMessage.NOTE_ON : ShortMessage.NOTE_OFF;
				int key = (shrutiOctave * 12) + shruti + keyOffset;

				ShortMessage message = new ShortMessage();
				message.setMessage(command, DRONE_CHANNEL, key, 127);

				MidiEvent event = new MidiEvent(message, tick);
				track.add(event);
			}

			droneSequencer.setSequence(sequence);
			droneSequencer.setLoopStartPoint(0);
			droneSequencer.setLoopEndPoint(-1);
			droneSequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
			droneSequencer.setTempoInBPM(droneTempo);
			
			droneSequencer.start();
		} catch (Exception e) {
			LOGGER.error("Error starting drone", e);
		}
	}

	public void stopDrone() {
		try {
			droneSequencer.stop();
		} catch (Exception e) {
			LOGGER.error("Error stopping drone", e);
		}
	}

	public void shutdown() {

		try {
			LOGGER.debug("Shutting down player");

			leadSequencer.stop();
			droneSequencer.stop();

			leadSequencer.close();
			droneSequencer.close();

			synthesizer.close();

		} catch (Exception e) {
			LOGGER.error("Error shutting down", e);
		}
	}

	public void stopLead() {
		leadSequencer.stop();
	}

}
