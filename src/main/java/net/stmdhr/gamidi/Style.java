package net.stmdhr.gamidi;

import javafx.geometry.Insets;

public class Style {

	public static final Insets PADDING_5 = new Insets(5);
	public static final Insets PADDING_SIDE_5 = new Insets(0, 5, 0, 5);
	public static final Insets PADDING_SIDE_10 = new Insets(0, 10, 0, 10);

}
