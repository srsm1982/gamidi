package net.stmdhr.gamidi;

import java.io.File;
import java.util.Arrays;

import javax.sound.midi.MidiSystem;
import javax.sound.midi.Patch;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;

import com.sun.media.sound.SF2Soundbank;

public class PlayerTest {

	private static Synthesizer synthesizer;
	private static Sequencer sequencer;

	public static void main(String[] args) {

		try {
			synthesizer = MidiSystem.getSynthesizer();
			sequencer = MidiSystem.getSequencer(false);

			synthesizer = MidiSystem.getSynthesizer();
			sequencer = MidiSystem.getSequencer(false);

			Transmitter transmitter = sequencer.getTransmitter();
			Receiver synthReceiver = synthesizer.getReceiver();
			transmitter.setReceiver(synthReceiver);

			synthesizer.open();
			sequencer.open();

			System.out.println(Arrays.toString(synthesizer.getLoadedInstruments()));

			SF2Soundbank tmbBank = new SF2Soundbank(
					new File("C:\\workspace\\jfx\\gamidi\\src\\main\\deploy\\Tamburas.sf2"));
			tmbBank.getInstruments()[0].setPatch(new Patch(128, 0));
			
			System.out.println(Arrays.toString(tmbBank.getInstruments()));

			synthesizer.loadInstruments(tmbBank, new Patch[]{new Patch(128, 0)});

			System.out.println(Arrays.toString(synthesizer.getLoadedInstruments()));

			sequencer.close();
			synthesizer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
