package net.stmdhr.gamidi;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;
import javax.sound.midi.Transmitter;

public class PortamentoTest {

	public static void main(String[] args) {
		try {
			
//			192,72,0,1,program-change
//			176,65,127,1,control-change
//			176,5,70,1,control-change
//			144,60,127,4,on
//			128,60,127,10,off
//			144,62,127,9,on
//			128,62,127,15,off
//			144,64,127,14,on
//			128,64,127,20,off
//			144,65,127,19,on
//			176,65,0,24,control-change
//			128,65,127,25,off
//			144,67,127,24,on
//			128,67,127,30,off
//			144,69,127,29,on
//			128,69,127,35,off
//			144,71,127,34,on
//			128,71,127,40,off
//			144,72,127,39,on
//			128,72,127,45,off


			FileReader fileReader = new FileReader("input.csv");
			BufferedReader reader = new BufferedReader(fileReader);

			Sequence sequence = new Sequence(Sequence.PPQ, 1);
			Track track = sequence.createTrack();
			
			String line = reader.readLine();
			while (line != null) {
				
				String[] split = line.split(",");
				
				int command = Integer.valueOf(split[0]);
				int data1 = Integer.valueOf(split[1]);
				int data2 = Integer.valueOf(split[2]);
				long tick = Long.valueOf(split[3]);

				ShortMessage message = new ShortMessage();
				message.setMessage(command, 0, data1, data2);

				MidiEvent event = new MidiEvent(message, tick);
				track.add(event);

				line = reader.readLine();
			}
			
			reader.close();

			Sequencer sequencer = MidiSystem.getSequencer(false);
			sequencer.open();
			sequencer.setSequence(sequence);
			sequencer.setTempoInBPM(500);

			Synthesizer synthesizer = MidiSystem.getSynthesizer();
			synthesizer.open();
			
			MidiChannel[] channels = synthesizer.getChannels();
			MidiChannel channel = channels[0];
			channel.setMono(true);

			Transmitter seqTransmitter = sequencer.getTransmitter();
			Receiver synthReceiver = synthesizer.getReceiver();
			seqTransmitter.setReceiver(synthReceiver);

			sequencer.addMetaEventListener(new MetaEventListener() {
				public void meta(MetaMessage event) {
					if (event.getType() == 47) {
						sequencer.close();
						if (synthesizer != null) {
							synthesizer.close();
						}
					}
				}
			});

			sequencer.start();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
