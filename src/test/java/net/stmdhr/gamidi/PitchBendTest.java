package net.stmdhr.gamidi;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;
import javax.sound.midi.Transmitter;

public class PitchBendTest {
	
	public static void main2(String[] args) {
		try {

			Synthesizer synth = MidiSystem.getSynthesizer();
			synth.open();
			
			MidiChannel[] channels = synth.getChannels();
			MidiChannel channel = channels[0];

			channel.setMono(true);

			channel.programChange(0, 40);
			
			channel.noteOn(55, 127);
			

			for (int i = 0; i < 10; i++) {
				channel.setPitchBend(8192 + (i * 500));
				Thread.sleep(500);
			}

			for (int i = 0; i < 10; i++) {
				channel.setPitchBend(8192 - (i * 500));
				Thread.sleep(500);
			}

			channel.noteOff(55);

			synth.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		try {

			int ticksPerQuarter = 100;
			Sequence sequence = new Sequence(Sequence.PPQ, ticksPerQuarter);
			Track track = sequence.createTrack();

			FileReader fileReader = new FileReader("input.csv");
			BufferedReader reader = new BufferedReader(fileReader);

			int[][] gamaka0 = { { 25, -1 }, { 50, 0 }, { 75, -1 }, { 99, 0 } };
			int[][] gamaka1 = { { 1, -1 }, { 50, 0 }, { 75, -1 }, { 99, 0 } };
			int[][][] gamakas = { gamaka0, gamaka1 };

			ShortMessage instrumentMessage = new ShortMessage();
			instrumentMessage.setMessage(ShortMessage.PROGRAM_CHANGE, 0, 71, 0);
			MidiEvent instrumentEvent = new MidiEvent(instrumentMessage, 0);
			track.add(instrumentEvent);
			
			ShortMessage monoMessage = new ShortMessage();
			monoMessage.setMessage(ShortMessage.CONTROL_CHANGE, 0, 126, 127);
			MidiEvent monoEvent = new MidiEvent(monoMessage, 0);
			track.add(monoEvent);
			
			ShortMessage pitchSensitivityMessage1 = new ShortMessage();
			pitchSensitivityMessage1.setMessage(ShortMessage.CONTROL_CHANGE, 0, 101, 0);
			MidiEvent pitchSensitityEvent1 = new MidiEvent(pitchSensitivityMessage1, 0);
			track.add(pitchSensitityEvent1);
			
			ShortMessage pitchSensitivityMessage2 = new ShortMessage();
			pitchSensitivityMessage2.setMessage(ShortMessage.CONTROL_CHANGE, 0, 100, 0);
			MidiEvent pitchSensitityEvent2 = new MidiEvent(pitchSensitivityMessage2, 0);
			track.add(pitchSensitityEvent2);
			
			ShortMessage pitchSensitivityMessage3 = new ShortMessage();
			pitchSensitivityMessage3.setMessage(ShortMessage.CONTROL_CHANGE, 0, 6, 6);
			MidiEvent pitchSensitityEvent3 = new MidiEvent(pitchSensitivityMessage3, 0);
			track.add(pitchSensitityEvent3);

			long currentBeat = 1;

			String line = reader.readLine();
			while (line != null) {

				String[] split = line.split(",");

				int note = Integer.valueOf(split[0]);
				int gamakaType = Integer.valueOf(split[1]);
				int duration = Integer.valueOf(split[2]);

				ShortMessage onMessage = new ShortMessage();
				onMessage.setMessage(ShortMessage.NOTE_ON, 0, note, 127);

				long startTick = currentBeat * ticksPerQuarter;
				MidiEvent onEvent = new MidiEvent(onMessage, startTick);
				track.add(onEvent);

				if (gamakaType != -1) {

					int[][] gamaka = gamakas[gamakaType];

					int leftTick = 0;
					int leftBend = 8192;

					int rightTick = 0;
					int rightBend = 8192;

					int bendGradient = 0;

					int currentGamakaPoint = 0;

					for (int tick = 0; tick < ticksPerQuarter; tick++) {

						if (tick > rightTick) {

							int[] point = gamaka[currentGamakaPoint++];

							leftTick = rightTick;
							leftBend = rightBend;

							rightTick = point[0];
							rightBend = 8192 + (point[1] * 8191) / 12;

							if (rightTick == leftTick) {
								bendGradient = 0;
							} else {
								bendGradient = (rightBend - leftBend) / (rightTick - leftTick);
							}

							tick--;

						} else {

							int bend = leftBend + (tick - leftTick) * bendGradient;
							int bendMSB = bend >> 7; // 7 left most bits
							int bendLSB = bend & 0b1111111; // 7 right most bits

							ShortMessage pitchMessage = new ShortMessage();
							pitchMessage.setMessage(ShortMessage.PITCH_BEND, 0, bendLSB, bendMSB);

							MidiEvent pitchEvent = new MidiEvent(pitchMessage, startTick + tick);
							track.add(pitchEvent);
						}

					}

				}

				currentBeat += duration;
				long endTick = currentBeat * ticksPerQuarter;

				ShortMessage offMessage = new ShortMessage();
				offMessage.setMessage(ShortMessage.NOTE_OFF, 0, note, 127);

				MidiEvent offEvent = new MidiEvent(offMessage, endTick);
				track.add(offEvent);
				
//				{
//					// Reset
//					int bendMSB = 64;
//					int bendLSB = 0;
//
//					ShortMessage pitchMessage = new ShortMessage();
//					pitchMessage.setMessage(ShortMessage.PITCH_BEND, 0, bendLSB, bendMSB);
//
//					MidiEvent pitchEvent = new MidiEvent(pitchMessage, endTick);
//					track.add(pitchEvent);
//				}

				line = reader.readLine();
			}

			reader.close();

			Sequencer sequencer = MidiSystem.getSequencer(false);
			sequencer.open();
			sequencer.setSequence(sequence);
			sequencer.setTempoInBPM(100);

			Synthesizer synthesizer = MidiSystem.getSynthesizer();
			synthesizer.open();

			MidiChannel[] channels = synthesizer.getChannels();
			MidiChannel channel = channels[0];
			channel.setMono(true);

//			channel.controlChange(101, 0);
//			channel.controlChange(100, 0);
//			channel.controlChange(6, 6);
//			channel.controlChange(38, 0);

			Transmitter seqTransmitter = sequencer.getTransmitter();
			Receiver synthReceiver = synthesizer.getReceiver();
			seqTransmitter.setReceiver(synthReceiver);

			sequencer.addMetaEventListener(new MetaEventListener() {
				public void meta(MetaMessage event) {
					if (event.getType() == 47) {
						sequencer.close();
						if (synthesizer != null) {
							synthesizer.close();
						}
					}
				}
			});

			sequencer.start();
			

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
